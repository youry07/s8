### A Pluto.jl notebook ###
# v0.17.5

using Markdown
using InteractiveUtils

# ╔═╡ 17faaa12-b010-11ec-3e6e-df6954e7df5e
using Revise

# ╔═╡ 39213521-2acf-480a-8cc8-d97a170bbac6
using PlutoUI

# ╔═╡ fbc00aed-f378-4555-ba39-8bfbfebc25e5
using EventSimulation

# ╔═╡ 22eff12e-289e-4fa6-a68f-30f774821639
using Random

# ╔═╡ 4cc36953-3c77-454e-bfdb-47563536ce1e
using Distributions

# ╔═╡ 86af5b21-79bb-49c4-9991-c342219033b9
using DataStructures

# ╔═╡ 95800905-ab7f-4f5a-a5f2-0f42e7f61368
TableOfContents()

# ╔═╡ d732380e-cc8d-4e9e-9591-642b6db9bb7e
md"""
 
## Тема: Дослідження за допомогою імітаційної моделі процесу розширення системи обслуговування з одним приладом і чергою

#### Мета: Освоєння принципів моделювання процесів функціонування систем, отримання і закріплення навичок побудови імітаційних моделей.

#### Виконав: Юрій Харченко

#### Варіант 4

##### В роботі використано пакети мови Julia - Pluto, EventSimulation
"""

# ╔═╡ dc1b7fbc-6ca5-4daa-b3bb-a66b93ab66a5
mutable struct Customer
	t:: Int
	arrival:: Float64
	departure:: Float64
	enqueue:: Float64
	dequeue:: Float64
end 

# ╔═╡ eeede991-4f82-4448-bdf2-21b86aa3fdd5
mutable struct CounterState <: AbstractState
    count::Int
	total_time::Float64
    customers::Vector{Customer}
	queue_len::Int   # queue length
    is_busy::Bool  # is server busy?
	queue::Queue
	max_queue:: Int
	serve::Int
end

# ╔═╡ bc884ace-2d13-4201-8ab2-b956f5b7f6e7
md"""
### Задача 1

На прийом до лікаря терапевта приходять пацієнти двох типів:

1) мають карту хвороб на руках і час їх приходу розподілено рівномірно в інтервалі [3,17] хв;

2) прийшли на прийом вперше, час їх приходу через [15,21] хв. 

Час прийому пацієнтів першого типу [12,16] хв, а другого типу - [15,25] хв.

Модель роботи лікаря повинна забезпечити збір статистики про чергу.

Необхідно промоделювати роботу лікаря впродовж 3 годин

"""

# ╔═╡ add04a31-a72b-4444-8295-c7987149d3d0
text01 = let

p = 3 * 60
Random.seed!(8)
a1 = Uniform(3.0, 17.0)
a2 = Uniform(15.0, 21.0)
s1 = Uniform(12.0, 16.0)
s2 = Uniform(15.0, 25.0)
	
log = []
	
arrival1 = (s) -> begin
	s.state.count += 1
	c = Customer( 1, s.now, p, 0.0, 0.0)
	push!(s.state.customers, c)
    if s.state.is_busy # server is working?
        c.enqueue = c.arrival
		c.dequeue = p
		enqueue!(s.state.queue, s.state.count)
		if length(s.state.queue) > s.state.max_queue
			s.state.max_queue = length(s.state.queue)
		end
    else
		s.state.serve = s.state.count 
        s.state.is_busy = true
		
        register!(s, departure, rand(s1))
    end
    
end

arrival2 = (s) -> begin
	s.state.count += 1
	c = Customer(2, s.now, p, 0.0, 0.0)
	push!(s.state.customers, c)
    if s.state.is_busy # server is working?
        c.enqueue = c.arrival
		c.dequeue = p
		enqueue!(s.state.queue, s.state.count)
		if length(s.state.queue) > s.state.max_queue
			s.state.max_queue = length(s.state.queue)
		end
    else
		s.state.serve = s.state.count 
        s.state.is_busy = true
		
        register!(s, departure, rand(s2))
    end
    
end

departure = (s) -> begin
	s.state.customers[s.state.serve].departure = s.now
	
    if length(s.state.queue) > 0 # any customers waiting?
        i = dequeue!(s.state.queue)
		s.state.serve = i
		s.state.customers[i].dequeue = s.now
		if s.state.customers[i] == 1
	    	register!(s, departure, rand(s1))
		else
			register!(s, departure, rand(s2))
		end
    else
        s.state.is_busy = false
    end
end

monitor = (s, Δ) -> begin
    s.state.total_time += Δ
end

cs = CounterState(0, 0.0, [], 0, false, Queue{Int}(), 0, 0)
s = Scheduler(cs, Float64, monitor)

repeat_register!(s, arrival1, x -> rand(a1))
repeat_register!(s, arrival2, x -> rand(a2))
go!(s, p)
	
push!(log, "Повний час моделі: $(round(cs.total_time, digits=3)) хв.")
push!(log, "Відвідало клієнтів: $(cs.count)")
aver_sys_time = sum([c.departure - c.arrival for c in cs.customers])/cs.count
push!(log, "Середній час клієнта в системі: $(round(aver_sys_time, digits=3)) хв.")
aver_q_time = sum([c.dequeue - c.enqueue for c in cs.customers])/cs.count
push!(log, "Середній час клієнта в черзі: $(round(aver_q_time, digits=3)) хв.")
max_q_time = maximum([c.dequeue - c.enqueue for c in cs.customers])
push!(log, "Максимальний час клієнта в черзі: $(round(max_q_time, digits=3)) хв.")	
push!(log, "Максимальний розмір черги: $(cs.max_queue)")
push!(log, "")
push!(log, "t\t  in\t  out\t  enq\t  deq")
for c in cs.customers
	push!(log, "$(c.t)\t $(round(c.arrival))\t $(round(c.departure))\t $(round(c.enqueue))\t $(round(c.dequeue))")
end
Text(join(log, '\n'))
	
end;

# ╔═╡ 4fcedbaa-2e18-440b-9039-57cac8eae806
text01

# ╔═╡ d40df599-171c-438e-b5c4-df08cd88b0be
md"""
### Задача 2

У бібліотеку приходять читачі двох типів: що прийшли в бібліотеку вперше і повторно. Інтервали приходу читачів першого типу розподілені рівномірно через [23,31] хвилин, другого - [22,44] хвилин.

Час роботи з читачами першого типу [7,33] хвилин, другого типу - [12,20] хвилин. 

Модель роботи бібліотекаря повинна забезпечити збір статистики про чергу.

Необхідно промоделювати роботу бібліотекаря впродовж 4 годин

"""

# ╔═╡ 0cb38496-d84f-4b5a-b29e-d230b3b71db7
text02 = let

p = 4 * 60
Random.seed!(8)
a1 = Uniform(23.0, 31.0)
a2 = Uniform(22.0, 44.0)
s1 = Uniform(7.0, 33.0)
s2 = Uniform(12.0, 20.0)


log = []
	
arrival1 = (s) -> begin
	s.state.count += 1
	c = Customer( 1, s.now, p, 0.0, 0.0)
	push!(s.state.customers, c)
    if s.state.is_busy # server is working?
        c.enqueue = c.arrival
		c.dequeue = p
		enqueue!(s.state.queue, s.state.count)
		if length(s.state.queue) > s.state.max_queue
			s.state.max_queue = length(s.state.queue)
		end
    else
		s.state.serve = s.state.count 
        s.state.is_busy = true
		
        register!(s, departure, rand(s1))
    end
    
end

arrival2 = (s) -> begin
	s.state.count += 1
	c = Customer(2, s.now, p, 0.0, 0.0)
	push!(s.state.customers, c)
    if s.state.is_busy # server is working?
        c.enqueue = c.arrival
		c.dequeue = p
		enqueue!(s.state.queue, s.state.count)
		if length(s.state.queue) > s.state.max_queue
			s.state.max_queue = length(s.state.queue)
		end
    else
		s.state.serve = s.state.count 
        s.state.is_busy = true
		
        register!(s, departure, rand(s2))
    end
    
end

departure = (s) -> begin
	s.state.customers[s.state.serve].departure = s.now
	
    if length(s.state.queue) > 0 # any customers waiting?
        i = dequeue!(s.state.queue)
		s.state.serve = i
		s.state.customers[i].dequeue = s.now
		if s.state.customers[i] == 1
	    	register!(s, departure, rand(s1))
		else
			register!(s, departure, rand(s2))
		end
    else
        s.state.is_busy = false
    end
end

monitor = (s, Δ) -> begin
    s.state.total_time += Δ
end

cs = CounterState(0, 0.0, [], 0, false, Queue{Int}(), 0, 0)
s = Scheduler(cs, Float64, monitor)

repeat_register!(s, arrival1, x -> rand(a1))
repeat_register!(s, arrival2, x -> rand(a2))
go!(s, p)
	
push!(log, "Повний час моделі: $(round(cs.total_time, digits=3)) хв.")
push!(log, "Відвідало клієнтів: $(cs.count)")
aver_sys_time = sum([c.departure - c.arrival for c in cs.customers])/cs.count
push!(log, "Середній час клієнта в системі: $(round(aver_sys_time, digits=3)) хв.")
aver_q_time = sum([c.dequeue - c.enqueue for c in cs.customers])/cs.count
push!(log, "Середній час клієнта в черзі: $(round(aver_q_time, digits=3)) хв.")
max_q_time = maximum([c.dequeue - c.enqueue for c in cs.customers])
push!(log, "Максимальний час клієнта в черзі: $(round(max_q_time, digits=3)) хв.")	
push!(log, "Максимальний розмір черги: $(cs.max_queue)")
push!(log, "")
push!(log, "t\t  in\t  out\t  enq\t  deq")
for c in cs.customers
	push!(log, "$(c.t)\t $(round(c.arrival))\t $(round(c.departure))\t $(round(c.enqueue))\t $(round(c.dequeue))")
end
Text(join(log, '\n'))
	
end;

# ╔═╡ cbb6a3a3-d214-4afc-90de-cf16c76578bf
text02

# ╔═╡ 29c668e2-1d21-4e76-be92-06bd68379e73
md"""
### Задача 3

В квиткову касу аерофлоту приходять пасажири двох типів : першого типу – що хочуть придбатиі авіаквитки; другого типу – що хочуть поміняти наявні у них авіаквитки. 

Прихід пасажирів першого типу розподілений рівномірно в інтервалі [4,12] хвилин, другого типу так само розподілений рівномірно в інтервалі [5,35] хвилин.

Час обслуговування пасажирів першого типу - [6,16] хвилин, а другого - [9,23] хвилин. 

Модель роботи квиткової каси аерофлоту повинна забезпечити збір статистики про чергу.

Необхідно промоделировать роботу каси впродовж 7 годин

"""

# ╔═╡ 203b4f8a-4cb6-4745-a84a-485fa00fb54e
text03 = let

p = 7 * 60
Random.seed!(8)
a1 = Uniform(4.0, 12.0)
a2 = Uniform(5.0, 35.0)
s1 = Uniform(6.0, 16.0)
s2 = Uniform(9.0, 23.0)


log = []
	
arrival1 = (s) -> begin
	s.state.count += 1
	c = Customer( 1, s.now, p, 0.0, 0.0)
	push!(s.state.customers, c)
    if s.state.is_busy # server is working?
        c.enqueue = c.arrival
		c.dequeue = p
		enqueue!(s.state.queue, s.state.count)
		if length(s.state.queue) > s.state.max_queue
			s.state.max_queue = length(s.state.queue)
		end
    else
		s.state.serve = s.state.count 
        s.state.is_busy = true
		
        register!(s, departure, rand(s1))
    end
    
end

arrival2 = (s) -> begin
	s.state.count += 1
	c = Customer(2, s.now, p, 0.0, 0.0)
	push!(s.state.customers, c)
    if s.state.is_busy # server is working?
        c.enqueue = c.arrival
		c.dequeue = p
		enqueue!(s.state.queue, s.state.count)
		if length(s.state.queue) > s.state.max_queue
			s.state.max_queue = length(s.state.queue)
		end
    else
		s.state.serve = s.state.count 
        s.state.is_busy = true
		
        register!(s, departure, rand(s2))
    end
    
end

departure = (s) -> begin
	s.state.customers[s.state.serve].departure = s.now
	
    if length(s.state.queue) > 0 # any customers waiting?
        i = dequeue!(s.state.queue)
		s.state.serve = i
		s.state.customers[i].dequeue = s.now
		if s.state.customers[i] == 1
	    	register!(s, departure, rand(s1))
		else
			register!(s, departure, rand(s2))
		end
    else
        s.state.is_busy = false
    end
end

monitor = (s, Δ) -> begin
    s.state.total_time += Δ
end

cs = CounterState(0, 0.0, [], 0, false, Queue{Int}(), 0, 0)
s = Scheduler(cs, Float64, monitor)

repeat_register!(s, arrival1, x -> rand(a1))
repeat_register!(s, arrival2, x -> rand(a2))
go!(s, p)
	
push!(log, "Повний час моделі: $(round(cs.total_time, digits=3)) хв.")
push!(log, "Відвідало клієнтів: $(cs.count)")
aver_sys_time = sum([c.departure - c.arrival for c in cs.customers])/cs.count
push!(log, "Середній час клієнта в системі: $(round(aver_sys_time, digits=3)) хв.")
aver_q_time = sum([c.dequeue - c.enqueue for c in cs.customers])/cs.count
push!(log, "Середній час клієнта в черзі: $(round(aver_q_time, digits=3)) хв.")
max_q_time = maximum([c.dequeue - c.enqueue for c in cs.customers])
push!(log, "Максимальний час клієнта в черзі: $(round(max_q_time, digits=3)) хв.")	
push!(log, "Максимальний розмір черги: $(cs.max_queue)")
push!(log, "")
push!(log, "t\t  in\t  out\t  enq\t  deq")
for c in cs.customers
	push!(log, "$(c.t)\t $(round(c.arrival))\t $(round(c.departure))\t $(round(c.enqueue))\t $(round(c.dequeue))")
end
Text(join(log, '\n'))
	
end;

# ╔═╡ 7bbff5d1-ff99-4771-a3fd-d875335a0203
text03

# ╔═╡ f7c3e64f-2644-4dfe-985f-e0963b19fa06
md"""
### Задача 4

В пункт обміну валюти приходять клієнти двох типів : 

1) купити валюту, інтервали приходу клієнтів розподілені рівномірно, [8,20] хвилин; 

2) здати одну валюту і купити іншу, їх прихід через [20,74] хвилин. 

Час обслуговування клієнтів першого типу також рівномірно розподілений по [6,12] хвилин, другого типу - по [10,22] хвилин. 

Модель роботи обмінного пункту повинна забезпечити збір статистики про чергу.

Необхідно промоделювати роботу пункту впродовж 6 годин

"""

# ╔═╡ 3c519396-4d61-4a0b-890a-7c722d94b149
text04 = let

p = 6 * 60
Random.seed!(8)
a1 = Uniform(8.0, 20.0)
a2 = Uniform(20.0, 74.0)
s1 = Uniform(6.0, 12.0)
s2 = Uniform(10.0, 22.0)


log = []
	
arrival1 = (s) -> begin
	s.state.count += 1
	c = Customer( 1, s.now, p, 0.0, 0.0)
	push!(s.state.customers, c)
    if s.state.is_busy # server is working?
        c.enqueue = c.arrival
		c.dequeue = p
		enqueue!(s.state.queue, s.state.count)
		if length(s.state.queue) > s.state.max_queue
			s.state.max_queue = length(s.state.queue)
		end
    else
		s.state.serve = s.state.count 
        s.state.is_busy = true
		
        register!(s, departure, rand(s1))
    end
    
end

arrival2 = (s) -> begin
	s.state.count += 1
	c = Customer(2, s.now, p, 0.0, 0.0)
	push!(s.state.customers, c)
    if s.state.is_busy # server is working?
        c.enqueue = c.arrival
		c.dequeue = p
		enqueue!(s.state.queue, s.state.count)
		if length(s.state.queue) > s.state.max_queue
			s.state.max_queue = length(s.state.queue)
		end
    else
		s.state.serve = s.state.count 
        s.state.is_busy = true
		
        register!(s, departure, rand(s2))
    end
    
end

departure = (s) -> begin
	s.state.customers[s.state.serve].departure = s.now
	
    if length(s.state.queue) > 0 # any customers waiting?
        i = dequeue!(s.state.queue)
		s.state.serve = i
		s.state.customers[i].dequeue = s.now
		if s.state.customers[i] == 1
	    	register!(s, departure, rand(s1))
		else
			register!(s, departure, rand(s2))
		end
    else
        s.state.is_busy = false
    end
end

monitor = (s, Δ) -> begin
    s.state.total_time += Δ
end

cs = CounterState(0, 0.0, [], 0, false, Queue{Int}(), 0, 0)
s = Scheduler(cs, Float64, monitor)

repeat_register!(s, arrival1, x -> rand(a1))
repeat_register!(s, arrival2, x -> rand(a2))
go!(s, p)
	
push!(log, "Повний час моделі: $(round(cs.total_time, digits=3)) хв.")
push!(log, "Відвідало клієнтів: $(cs.count)")
aver_sys_time = sum([c.departure - c.arrival for c in cs.customers])/cs.count
push!(log, "Середній час клієнта в системі: $(round(aver_sys_time, digits=3)) хв.")
aver_q_time = sum([c.dequeue - c.enqueue for c in cs.customers])/cs.count
push!(log, "Середній час клієнта в черзі: $(round(aver_q_time, digits=3)) хв.")
max_q_time = maximum([c.dequeue - c.enqueue for c in cs.customers])
push!(log, "Максимальний час клієнта в черзі: $(round(max_q_time, digits=3)) хв.")	
push!(log, "Максимальний розмір черги: $(cs.max_queue)")
push!(log, "")
push!(log, "t\t  in\t  out\t  enq\t  deq")
for c in cs.customers
	push!(log, "$(c.t)\t $(round(c.arrival))\t $(round(c.departure))\t $(round(c.enqueue))\t $(round(c.dequeue))")
end
Text(join(log, '\n'))
	
end;

# ╔═╡ 43352332-a659-465c-8de3-8b1948255f3c
text04

# ╔═╡ e626dd46-39f9-4556-8d76-7f47c0d54157
md"""
### Задача 5

На пошту з 1 вікном для прийому телеграм приходять клієнти двох типів:

1) дати телеграму в межах країни, інтервали приходу клієнтів розподілені рівномірно в інтервалі [2,14] хвилин;

2) дати телеграму за рубіж, їх прихід через [20,70] хвилин. 

Час прийому телеграм у клієнтів першого типу також розподілений рівномірно по [5,9] хвилин, другого типу - по [5,13] хвилин.

Модель роботи вікна прийому телеграм повинна забезпечити збір статистики про чергу.

Необхідно промоделювати роботу вікна прийому телеграм впродовж 12 годин

"""

# ╔═╡ e129048f-fe44-4c87-b341-2b41c49f73eb
text05 = let

p = 12 * 60
Random.seed!(8)
a1 = Uniform(2.0, 14.0)
a2 = Uniform(20.0, 70.0)
s1 = Uniform(5.0, 9.0)
s2 = Uniform(5.0, 13.0)


log = []
	
arrival1 = (s) -> begin
	s.state.count += 1
	c = Customer( 1, s.now, p, 0.0, 0.0)
	push!(s.state.customers, c)
    if s.state.is_busy # server is working?
        c.enqueue = c.arrival
		c.dequeue = p
		enqueue!(s.state.queue, s.state.count)
		if length(s.state.queue) > s.state.max_queue
			s.state.max_queue = length(s.state.queue)
		end
    else
		s.state.serve = s.state.count 
        s.state.is_busy = true
		
        register!(s, departure, rand(s1))
    end
    
end

arrival2 = (s) -> begin
	s.state.count += 1
	c = Customer(2, s.now, p, 0.0, 0.0)
	push!(s.state.customers, c)
    if s.state.is_busy # server is working?
        c.enqueue = c.arrival
		c.dequeue = p
		enqueue!(s.state.queue, s.state.count)
		if length(s.state.queue) > s.state.max_queue
			s.state.max_queue = length(s.state.queue)
		end
    else
		s.state.serve = s.state.count 
        s.state.is_busy = true
		
        register!(s, departure, rand(s2))
    end
    
end

departure = (s) -> begin
	s.state.customers[s.state.serve].departure = s.now
	
    if length(s.state.queue) > 0 # any customers waiting?
        i = dequeue!(s.state.queue)
		s.state.serve = i
		s.state.customers[i].dequeue = s.now
		if s.state.customers[i] == 1
	    	register!(s, departure, rand(s1))
		else
			register!(s, departure, rand(s2))
		end
    else
        s.state.is_busy = false
    end
end

monitor = (s, Δ) -> begin
    s.state.total_time += Δ
end

cs = CounterState(0, 0.0, [], 0, false, Queue{Int}(), 0, 0)
s = Scheduler(cs, Float64, monitor)

repeat_register!(s, arrival1, x -> rand(a1))
repeat_register!(s, arrival2, x -> rand(a2))
go!(s, p)
	
push!(log, "Повний час моделі: $(round(cs.total_time, digits=3)) хв.")
push!(log, "Відвідало клієнтів: $(cs.count)")
aver_sys_time = sum([c.departure - c.arrival for c in cs.customers])/cs.count
push!(log, "Середній час клієнта в системі: $(round(aver_sys_time, digits=3)) хв.")
aver_q_time = sum([c.dequeue - c.enqueue for c in cs.customers])/cs.count
push!(log, "Середній час клієнта в черзі: $(round(aver_q_time, digits=3)) хв.")
max_q_time = maximum([c.dequeue - c.enqueue for c in cs.customers])
push!(log, "Максимальний час клієнта в черзі: $(round(max_q_time, digits=3)) хв.")	
push!(log, "Максимальний розмір черги: $(cs.max_queue)")
push!(log, "")
push!(log, "t\t  in\t  out\t  enq\t  deq")
for c in cs.customers
	push!(log, "$(c.t)\t $(round(c.arrival))\t $(round(c.departure))\t $(round(c.enqueue))\t $(round(c.dequeue))")
end
Text(join(log, '\n'))
	
end;

# ╔═╡ 64ab0ce0-2d43-472a-b3d9-da48bc883bf4
text05

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
DataStructures = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
Distributions = "31c24e10-a181-5473-b8eb-7969acd0382f"
EventSimulation = "bf752c2e-6b1c-53e3-a22f-e5bb209bd3f1"
PlutoUI = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
Random = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"
Revise = "295af30f-e4ad-537b-8983-00126c2a3abe"

[compat]
DataStructures = "~0.18.11"
Distributions = "~0.25.53"
EventSimulation = "~0.7.3"
PlutoUI = "~0.7.38"
Revise = "~3.3.3"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.1"
manifest_format = "2.0"

[[deps.AbstractPlutoDingetjes]]
deps = ["Pkg"]
git-tree-sha1 = "8eaf9f1b4921132a4cff3f36a1d9ba923b14a481"
uuid = "6e696c72-6542-2067-7265-42206c756150"
version = "1.1.4"

[[deps.ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.Calculus]]
deps = ["LinearAlgebra"]
git-tree-sha1 = "f641eb0a4f00c343bbc32346e1217b86f3ce9dad"
uuid = "49dc2e85-a5d0-5ad3-a950-438e2897f1b9"
version = "0.5.1"

[[deps.ChainRulesCore]]
deps = ["Compat", "LinearAlgebra", "SparseArrays"]
git-tree-sha1 = "9950387274246d08af38f6eef8cb5480862a435f"
uuid = "d360d2e6-b24c-11e9-a2a3-2a2ae2dbcce4"
version = "1.14.0"

[[deps.ChangesOfVariables]]
deps = ["ChainRulesCore", "LinearAlgebra", "Test"]
git-tree-sha1 = "bf98fa45a0a4cee295de98d4c1462be26345b9a1"
uuid = "9e997f8a-9a97-42d5-a9f1-ce6bfc15e2c0"
version = "0.1.2"

[[deps.CodeTracking]]
deps = ["InteractiveUtils", "UUIDs"]
git-tree-sha1 = "9fb640864691a0936f94f89150711c36072b0e8f"
uuid = "da1fd8a2-8d9e-5ec2-8556-3022fb5608a2"
version = "1.0.8"

[[deps.ColorTypes]]
deps = ["FixedPointNumbers", "Random"]
git-tree-sha1 = "024fe24d83e4a5bf5fc80501a314ce0d1aa35597"
uuid = "3da002f7-5984-5a60-b8a6-cbb66c0b333f"
version = "0.11.0"

[[deps.Compat]]
deps = ["Base64", "Dates", "DelimitedFiles", "Distributed", "InteractiveUtils", "LibGit2", "Libdl", "LinearAlgebra", "Markdown", "Mmap", "Pkg", "Printf", "REPL", "Random", "SHA", "Serialization", "SharedArrays", "Sockets", "SparseArrays", "Statistics", "Test", "UUIDs", "Unicode"]
git-tree-sha1 = "96b0bc6c52df76506efc8a441c6cf1adcb1babc4"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "3.42.0"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[deps.DataAPI]]
git-tree-sha1 = "cc70b17275652eb47bc9e5f81635981f13cea5c8"
uuid = "9a962f9c-6df0-11e9-0e5d-c546b8b5ee8a"
version = "1.9.0"

[[deps.DataStructures]]
deps = ["Compat", "InteractiveUtils", "OrderedCollections"]
git-tree-sha1 = "3daef5523dd2e769dad2365274f760ff5f282c7d"
uuid = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
version = "0.18.11"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.DelimitedFiles]]
deps = ["Mmap"]
uuid = "8bb1440f-4735-579b-a4ab-409b98df4dab"

[[deps.DensityInterface]]
deps = ["InverseFunctions", "Test"]
git-tree-sha1 = "80c3e8639e3353e5d2912fb3a1916b8455e2494b"
uuid = "b429d917-457f-4dbc-8f4c-0cc954292b1d"
version = "0.4.0"

[[deps.Distributed]]
deps = ["Random", "Serialization", "Sockets"]
uuid = "8ba89e20-285c-5b6f-9357-94700520ee1b"

[[deps.Distributions]]
deps = ["ChainRulesCore", "DensityInterface", "FillArrays", "LinearAlgebra", "PDMats", "Printf", "QuadGK", "Random", "SparseArrays", "SpecialFunctions", "Statistics", "StatsBase", "StatsFuns", "Test"]
git-tree-sha1 = "5a4168170ede913a2cd679e53c2123cb4b889795"
uuid = "31c24e10-a181-5473-b8eb-7969acd0382f"
version = "0.25.53"

[[deps.DocStringExtensions]]
deps = ["LibGit2"]
git-tree-sha1 = "b19534d1895d702889b219c382a6e18010797f0b"
uuid = "ffbed154-4ef7-542d-bbb7-c09d3a79fcae"
version = "0.8.6"

[[deps.Downloads]]
deps = ["ArgTools", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"

[[deps.DualNumbers]]
deps = ["Calculus", "NaNMath", "SpecialFunctions"]
git-tree-sha1 = "5837a837389fccf076445fce071c8ddaea35a566"
uuid = "fa6b7ba4-c1ee-5f82-b5fc-ecf0adba8f74"
version = "0.6.8"

[[deps.EventSimulation]]
deps = ["Random"]
git-tree-sha1 = "4214d8da65619059f25eb29db166c129ad515266"
uuid = "bf752c2e-6b1c-53e3-a22f-e5bb209bd3f1"
version = "0.7.3"

[[deps.FileWatching]]
uuid = "7b1f6079-737a-58dc-b8bc-7a2ca5c1b5ee"

[[deps.FillArrays]]
deps = ["LinearAlgebra", "Random", "SparseArrays", "Statistics"]
git-tree-sha1 = "246621d23d1f43e3b9c368bf3b72b2331a27c286"
uuid = "1a297f60-69ca-5386-bcde-b61e274b549b"
version = "0.13.2"

[[deps.FixedPointNumbers]]
deps = ["Statistics"]
git-tree-sha1 = "335bfdceacc84c5cdf16aadc768aa5ddfc5383cc"
uuid = "53c48c17-4a7d-5ca2-90c5-79b7896eea93"
version = "0.8.4"

[[deps.HypergeometricFunctions]]
deps = ["DualNumbers", "LinearAlgebra", "SpecialFunctions", "Test"]
git-tree-sha1 = "65e4589030ef3c44d3b90bdc5aac462b4bb05567"
uuid = "34004b35-14d8-5ef3-9330-4cdb6864b03a"
version = "0.3.8"

[[deps.Hyperscript]]
deps = ["Test"]
git-tree-sha1 = "8d511d5b81240fc8e6802386302675bdf47737b9"
uuid = "47d2ed2b-36de-50cf-bf87-49c2cf4b8b91"
version = "0.0.4"

[[deps.HypertextLiteral]]
git-tree-sha1 = "2b078b5a615c6c0396c77810d92ee8c6f470d238"
uuid = "ac1192a8-f4b3-4bfe-ba22-af5b92cd3ab2"
version = "0.9.3"

[[deps.IOCapture]]
deps = ["Logging", "Random"]
git-tree-sha1 = "f7be53659ab06ddc986428d3a9dcc95f6fa6705a"
uuid = "b5f81e59-6552-4d32-b1f0-c071b021bf89"
version = "0.2.2"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.InverseFunctions]]
deps = ["Test"]
git-tree-sha1 = "91b5dcf362c5add98049e6c29ee756910b03051d"
uuid = "3587e190-3f89-42d0-90ee-14403ec27112"
version = "0.1.3"

[[deps.IrrationalConstants]]
git-tree-sha1 = "7fd44fd4ff43fc60815f8e764c0f352b83c49151"
uuid = "92d709cd-6900-40b7-9082-c6be49f344b6"
version = "0.1.1"

[[deps.JLLWrappers]]
deps = ["Preferences"]
git-tree-sha1 = "abc9885a7ca2052a736a600f7fa66209f96506e1"
uuid = "692b3bcd-3c85-4b1f-b108-f13ce0eb3210"
version = "1.4.1"

[[deps.JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "3c837543ddb02250ef42f4738347454f95079d4e"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.3"

[[deps.JuliaInterpreter]]
deps = ["CodeTracking", "InteractiveUtils", "Random", "UUIDs"]
git-tree-sha1 = "9c43a2eb47147a8776ca2ba489f15a9f6f2906f8"
uuid = "aa1ae85d-cabe-5617-a682-6adf51b2e16a"
version = "0.9.11"

[[deps.LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"

[[deps.LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"

[[deps.LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[deps.LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.LogExpFunctions]]
deps = ["ChainRulesCore", "ChangesOfVariables", "DocStringExtensions", "InverseFunctions", "IrrationalConstants", "LinearAlgebra"]
git-tree-sha1 = "58f25e56b706f95125dcb796f39e1fb01d913a71"
uuid = "2ab3a3ac-af41-5b50-aa03-7779005ae688"
version = "0.3.10"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.LoweredCodeUtils]]
deps = ["JuliaInterpreter"]
git-tree-sha1 = "6b0440822974cab904c8b14d79743565140567f6"
uuid = "6f1432cf-f94c-5a45-995e-cdbf5db27b0b"
version = "2.2.1"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"

[[deps.Missings]]
deps = ["DataAPI"]
git-tree-sha1 = "bf210ce90b6c9eed32d25dbcae1ebc565df2687f"
uuid = "e1d29d7a-bbdc-5cf2-9ac0-f12de2c33e28"
version = "1.0.2"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"

[[deps.NaNMath]]
git-tree-sha1 = "737a5957f387b17e74d4ad2f440eb330b39a62c5"
uuid = "77ba4419-2d1f-58cd-9bb1-8ffee604a2e3"
version = "1.0.0"

[[deps.NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[deps.OpenLibm_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "05823500-19ac-5b8b-9628-191a04bc5112"

[[deps.OpenSpecFun_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "13652491f6856acfd2db29360e1bbcd4565d04f1"
uuid = "efe28fd5-8261-553b-a9e1-b2916fc3738e"
version = "0.5.5+0"

[[deps.OrderedCollections]]
git-tree-sha1 = "85f8e6578bf1f9ee0d11e7bb1b1456435479d47c"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.4.1"

[[deps.PDMats]]
deps = ["LinearAlgebra", "SparseArrays", "SuiteSparse"]
git-tree-sha1 = "e8185b83b9fc56eb6456200e873ce598ebc7f262"
uuid = "90014a1f-27ba-587c-ab20-58faa44d9150"
version = "0.11.7"

[[deps.Parsers]]
deps = ["Dates"]
git-tree-sha1 = "85b5da0fa43588c75bb1ff986493443f821c70b7"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "2.2.3"

[[deps.Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"

[[deps.PlutoUI]]
deps = ["AbstractPlutoDingetjes", "Base64", "ColorTypes", "Dates", "Hyperscript", "HypertextLiteral", "IOCapture", "InteractiveUtils", "JSON", "Logging", "Markdown", "Random", "Reexport", "UUIDs"]
git-tree-sha1 = "670e559e5c8e191ded66fa9ea89c97f10376bb4c"
uuid = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
version = "0.7.38"

[[deps.Preferences]]
deps = ["TOML"]
git-tree-sha1 = "d3538e7f8a790dc8903519090857ef8e1283eecd"
uuid = "21216c6a-2e73-6563-6e65-726566657250"
version = "1.2.5"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.QuadGK]]
deps = ["DataStructures", "LinearAlgebra"]
git-tree-sha1 = "78aadffb3efd2155af139781b8a8df1ef279ea39"
uuid = "1fd47b50-473d-5c70-9696-f719f8f3bcdc"
version = "2.4.2"

[[deps.REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.Reexport]]
git-tree-sha1 = "45e428421666073eab6f2da5c9d310d99bb12f9b"
uuid = "189a3867-3050-52da-a836-e630ba90ab69"
version = "1.2.2"

[[deps.Requires]]
deps = ["UUIDs"]
git-tree-sha1 = "838a3a4188e2ded87a4f9f184b4b0d78a1e91cb7"
uuid = "ae029012-a4dd-5104-9daa-d747884805df"
version = "1.3.0"

[[deps.Revise]]
deps = ["CodeTracking", "Distributed", "FileWatching", "JuliaInterpreter", "LibGit2", "LoweredCodeUtils", "OrderedCollections", "Pkg", "REPL", "Requires", "UUIDs", "Unicode"]
git-tree-sha1 = "4d4239e93531ac3e7ca7e339f15978d0b5149d03"
uuid = "295af30f-e4ad-537b-8983-00126c2a3abe"
version = "3.3.3"

[[deps.Rmath]]
deps = ["Random", "Rmath_jll"]
git-tree-sha1 = "bf3188feca147ce108c76ad82c2792c57abe7b1f"
uuid = "79098fc4-a85e-5d69-aa6a-4863f24498fa"
version = "0.7.0"

[[deps.Rmath_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "68db32dff12bb6127bac73c209881191bf0efbb7"
uuid = "f50d1b31-88e8-58de-be2c-1cc44531875f"
version = "0.3.0+0"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.SharedArrays]]
deps = ["Distributed", "Mmap", "Random", "Serialization"]
uuid = "1a1011a3-84de-559e-8e89-a11a2f7dc383"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SortingAlgorithms]]
deps = ["DataStructures"]
git-tree-sha1 = "b3363d7460f7d098ca0912c69b082f75625d7508"
uuid = "a2af1166-a08f-5f64-846c-94a0d3cef48c"
version = "1.0.1"

[[deps.SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.SpecialFunctions]]
deps = ["ChainRulesCore", "IrrationalConstants", "LogExpFunctions", "OpenLibm_jll", "OpenSpecFun_jll"]
git-tree-sha1 = "5ba658aeecaaf96923dce0da9e703bd1fe7666f9"
uuid = "276daf66-3868-5448-9aa4-cd146d93841b"
version = "2.1.4"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[deps.StatsAPI]]
deps = ["LinearAlgebra"]
git-tree-sha1 = "c3d8ba7f3fa0625b062b82853a7d5229cb728b6b"
uuid = "82ae8749-77ed-4fe6-ae5f-f523153014b0"
version = "1.2.1"

[[deps.StatsBase]]
deps = ["DataAPI", "DataStructures", "LinearAlgebra", "LogExpFunctions", "Missings", "Printf", "Random", "SortingAlgorithms", "SparseArrays", "Statistics", "StatsAPI"]
git-tree-sha1 = "8977b17906b0a1cc74ab2e3a05faa16cf08a8291"
uuid = "2913bbd2-ae8a-5f71-8c99-4fb6c76f3a91"
version = "0.33.16"

[[deps.StatsFuns]]
deps = ["ChainRulesCore", "HypergeometricFunctions", "InverseFunctions", "IrrationalConstants", "LogExpFunctions", "Reexport", "Rmath", "SpecialFunctions"]
git-tree-sha1 = "25405d7016a47cf2bd6cd91e66f4de437fd54a07"
uuid = "4c63d2b9-4356-54db-8cca-17b64c39e42c"
version = "0.9.16"

[[deps.SuiteSparse]]
deps = ["Libdl", "LinearAlgebra", "Serialization", "SparseArrays"]
uuid = "4607b0f0-06f3-5cda-b6b1-a6196a1729e9"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"

[[deps.Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"

[[deps.Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"

[[deps.nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"

[[deps.p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
"""

# ╔═╡ Cell order:
# ╠═17faaa12-b010-11ec-3e6e-df6954e7df5e
# ╠═39213521-2acf-480a-8cc8-d97a170bbac6
# ╠═95800905-ab7f-4f5a-a5f2-0f42e7f61368
# ╟─d732380e-cc8d-4e9e-9591-642b6db9bb7e
# ╠═fbc00aed-f378-4555-ba39-8bfbfebc25e5
# ╠═22eff12e-289e-4fa6-a68f-30f774821639
# ╠═4cc36953-3c77-454e-bfdb-47563536ce1e
# ╠═86af5b21-79bb-49c4-9991-c342219033b9
# ╠═dc1b7fbc-6ca5-4daa-b3bb-a66b93ab66a5
# ╠═eeede991-4f82-4448-bdf2-21b86aa3fdd5
# ╟─bc884ace-2d13-4201-8ab2-b956f5b7f6e7
# ╠═add04a31-a72b-4444-8295-c7987149d3d0
# ╠═4fcedbaa-2e18-440b-9039-57cac8eae806
# ╟─d40df599-171c-438e-b5c4-df08cd88b0be
# ╠═0cb38496-d84f-4b5a-b29e-d230b3b71db7
# ╠═cbb6a3a3-d214-4afc-90de-cf16c76578bf
# ╟─29c668e2-1d21-4e76-be92-06bd68379e73
# ╠═203b4f8a-4cb6-4745-a84a-485fa00fb54e
# ╠═7bbff5d1-ff99-4771-a3fd-d875335a0203
# ╟─f7c3e64f-2644-4dfe-985f-e0963b19fa06
# ╠═3c519396-4d61-4a0b-890a-7c722d94b149
# ╠═43352332-a659-465c-8de3-8b1948255f3c
# ╟─e626dd46-39f9-4556-8d76-7f47c0d54157
# ╠═e129048f-fe44-4c87-b341-2b41c49f73eb
# ╠═64ab0ce0-2d43-472a-b3d9-da48bc883bf4
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
