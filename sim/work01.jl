### A Pluto.jl notebook ###
# v0.17.5

using Markdown
using InteractiveUtils

# ╔═╡ cb8126fa-d921-4bb0-96bc-b8c8befe755e
using Revise

# ╔═╡ 889bf305-832d-4480-acc8-5a10a4b2fdd2
using EventSimulation

# ╔═╡ 2e8c6f59-e977-46b8-88f5-bd1e633d4a96
using Random

# ╔═╡ 7f8c4714-8a89-11ec-316a-6b89c5433917
md"""
# 
### Тема: Створення об’єктів «модель», «процес моделювання».

#### Мета: Ознайомитися з пакетом EventSimulation

#### Виконав: Юрій Харченко

##### В роботі використано пакети мови Julia - Pluto, EventSimulation
"""

# ╔═╡ 810532cd-cf8d-48d6-97d7-e5888fe1f89c
md"""
П’ять клієнтів прибувають в моменти 1, 2, ..., 5 і залишаються в системі протягом 1.5 одиниць часу
"""

# ╔═╡ 146cf49b-fdc3-4e2a-b7fc-55380bf5dd91
let
	
log = []
arrival = (s) -> begin
	t = s.now
	push!(log, "З'явився в $(s.now)")
    register!(s, x -> push!(log, "Пішов в $(x.now) (той, який з'явився в $t)"), 1.5)
end
s = Scheduler()
for t in 1.0:5.0
    register!(s, arrival, t)
end
go!(s)
Text(join(log, '\n'))
	
end

# ╔═╡ 2303c534-2853-4296-a13e-7ed9f5d22602
md"""
Заплановано багаторазове розміщення в черзі подій з дельтою часу, визначеною анонімною функцією x -> 1.0.

Другий аргумент 10 функції go! примушує безумовне припинення моделювання після цього моменту. 
"""

# ╔═╡ 9cd1471e-3422-42c7-bb88-d4e81dbb36a1
let
	
log = []
arrival = (s) -> begin
	t = s.now
	push!(log, "З'явився в $(s.now)")
    register!(s, x -> push!(log, "Пішов в $(x.now) (той, який з'явився в $t)"), 1.3)
end
s = Scheduler()
repeat_register!(s, arrival, x -> 1.2)
go!(s, 10)
Text(join(log, '\n'))
	
end

# ╔═╡ b854e76f-ca5a-42f0-b665-eee666fb32ef
md"""
Всі функції отримують Scheduler як аргумент, і тому вони мають доступ до поточного часу моделювання та стану моделювання.
"""

# ╔═╡ 2a3dd62a-9e2c-4bb0-a77b-24ced171b7c3
let
	
mutable struct CounterState <: AbstractState
    count::Int
end
	
log = []
arrival = (s) -> begin
	departure = (x) -> begin
        x.state.count -= 1
        push!(log, "Пішов в $(x.now), який з'явився в $t, залишилось $(x.state.count)")
    end
	t = s.now
	push!(log, "З'явився в $(s.now), було $(s.state.count)")
    register!(s, departure, 1.0)
    s.state.count += 1
end
s = Scheduler(CounterState(0))
Random.seed!(1)
repeat_register!(s, arrival, x -> rand())
go!(s, 15)
Text(join(log, '\n'))
	
end

# ╔═╡ 7c32b482-c8e9-4222-9ca4-81b7952efa26
md"""
Монітор змінює стан симуляції, щоб зібрати статистику моделювання.
"""

# ╔═╡ d439c89d-a16a-4225-b41a-8bf77c37abbc
let
	
mutable struct CounterState2 <: AbstractState
    count::Int
	total_time::Float64
    customer_time::Float64
end
	
log = []
arrival = (s) -> begin
    register!(s, departure, 1.0)
    s.state.count += 1
end

departure = (s) -> begin
    s.state.count -= 1
end

monitor = (s, Δ) -> begin
    s.state.total_time += Δ
    s.state.customer_time += Δ * s.state.count
end

cs = CounterState2(0, 0.0, 0.0)
s = Scheduler(cs, Float64, monitor)
Random.seed!(2)
repeat_register!(s, arrival, x -> rand())
go!(s, 100_000)
push!(log, "Середній час клієнта в системі: $(cs.customer_time/cs.total_time)")
Text(join(log, '\n'))
	
end

# ╔═╡ 8a2601e6-54c0-4488-be43-a988b00e572a
md"""
Два потоки: постачальник і клієнти. Постачальник надає одну одиницю товару кожну одиницю часу. Є клієнти, які приходять випадково і хочуть купити випадкову кількість товару. У черзі можуть стояти максимум два клієнти. 
"""

# ╔═╡ ed651752-39e3-4d79-ad76-1fb4ae92f4bc
let
	
mutable struct GoodState <: AbstractState
    good::SimResource{Float64}
end
	
log = []
delivery = (s) -> begin
    provide!(s, s.state.good, 1.0)
    push!(log, "Поставлено 1.0 в $(s.now)")
end

customer = (s) -> begin
	
    leave = (x) -> begin
        push!(log, "Пішов в $(x.now) з кількістю $(round(demand, digits=4)) од.")
    end
	
    demand = rand()
    push!(log, "З'явився в $(round(s.now, digits=4)) з потребою $(round(demand, digits=4)) од.")
    if !request!(s, s.state.good, demand, leave)[1]
        push!(log, "    але черга була занадто довгою (пішов порожнім)")
    else
        push!(log, "    і став в чергу")
    end
end

balance = (s) -> begin
    push!(log, "Кількість товарів на складі: $(round(s.state.good.quantity, digits=4)) од. в $(s.now)")
end

s = Scheduler(GoodState(SimResource{Float64}(max_requests=2)))
Random.seed!(1)
repeat_register!(s, delivery, x -> 1.0)
repeat_register!(s, customer, x -> rand())
repeat_register!(s, balance, x -> x.now == 0 ? 1.000001 : 1.0)
go!(s, 10)
Text(join(log, '\n'))
	
end

# ╔═╡ c2a0ff31-8444-4e59-a230-a595d4d42b59
md"""
#### Висновок

Пакет мови Julia EventSimulation може бути використаний для моделювання процесів масового обслуговування.

В наступних роботах будуть детальніше продемонстровані методи та структури пакету.
"""

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
EventSimulation = "bf752c2e-6b1c-53e3-a22f-e5bb209bd3f1"
Random = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"
Revise = "295af30f-e4ad-537b-8983-00126c2a3abe"

[compat]
EventSimulation = "~0.7.3"
Revise = "~3.3.1"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.1"
manifest_format = "2.0"

[[deps.ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.CodeTracking]]
deps = ["InteractiveUtils", "UUIDs"]
git-tree-sha1 = "9aa8a5ebb6b5bf469a7e0e2b5202cf6f8c291104"
uuid = "da1fd8a2-8d9e-5ec2-8556-3022fb5608a2"
version = "1.0.6"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.Distributed]]
deps = ["Random", "Serialization", "Sockets"]
uuid = "8ba89e20-285c-5b6f-9357-94700520ee1b"

[[deps.Downloads]]
deps = ["ArgTools", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"

[[deps.EventSimulation]]
deps = ["Random"]
git-tree-sha1 = "4214d8da65619059f25eb29db166c129ad515266"
uuid = "bf752c2e-6b1c-53e3-a22f-e5bb209bd3f1"
version = "0.7.3"

[[deps.FileWatching]]
uuid = "7b1f6079-737a-58dc-b8bc-7a2ca5c1b5ee"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.JuliaInterpreter]]
deps = ["CodeTracking", "InteractiveUtils", "Random", "UUIDs"]
git-tree-sha1 = "b55aae9a2bf436fc797d9c253a900913e0e90178"
uuid = "aa1ae85d-cabe-5617-a682-6adf51b2e16a"
version = "0.9.3"

[[deps.LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"

[[deps.LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"

[[deps.LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[deps.LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.LoweredCodeUtils]]
deps = ["JuliaInterpreter"]
git-tree-sha1 = "6b0440822974cab904c8b14d79743565140567f6"
uuid = "6f1432cf-f94c-5a45-995e-cdbf5db27b0b"
version = "2.2.1"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"

[[deps.MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"

[[deps.NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"

[[deps.OrderedCollections]]
git-tree-sha1 = "85f8e6578bf1f9ee0d11e7bb1b1456435479d47c"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.4.1"

[[deps.Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.Requires]]
deps = ["UUIDs"]
git-tree-sha1 = "838a3a4188e2ded87a4f9f184b4b0d78a1e91cb7"
uuid = "ae029012-a4dd-5104-9daa-d747884805df"
version = "1.3.0"

[[deps.Revise]]
deps = ["CodeTracking", "Distributed", "FileWatching", "JuliaInterpreter", "LibGit2", "LoweredCodeUtils", "OrderedCollections", "Pkg", "REPL", "Requires", "UUIDs", "Unicode"]
git-tree-sha1 = "2f9d4d6679b5f0394c52731db3794166f49d5131"
uuid = "295af30f-e4ad-537b-8983-00126c2a3abe"
version = "3.3.1"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"

[[deps.Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"

[[deps.nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"

[[deps.p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
"""

# ╔═╡ Cell order:
# ╠═cb8126fa-d921-4bb0-96bc-b8c8befe755e
# ╠═7f8c4714-8a89-11ec-316a-6b89c5433917
# ╠═889bf305-832d-4480-acc8-5a10a4b2fdd2
# ╟─810532cd-cf8d-48d6-97d7-e5888fe1f89c
# ╠═146cf49b-fdc3-4e2a-b7fc-55380bf5dd91
# ╟─2303c534-2853-4296-a13e-7ed9f5d22602
# ╠═9cd1471e-3422-42c7-bb88-d4e81dbb36a1
# ╠═2e8c6f59-e977-46b8-88f5-bd1e633d4a96
# ╟─b854e76f-ca5a-42f0-b665-eee666fb32ef
# ╠═2a3dd62a-9e2c-4bb0-a77b-24ced171b7c3
# ╟─7c32b482-c8e9-4222-9ca4-81b7952efa26
# ╠═d439c89d-a16a-4225-b41a-8bf77c37abbc
# ╟─8a2601e6-54c0-4488-be43-a988b00e572a
# ╠═ed651752-39e3-4d79-ad76-1fb4ae92f4bc
# ╠═c2a0ff31-8444-4e59-a230-a595d4d42b59
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
