### A Pluto.jl notebook ###
# v0.17.5

using Markdown
using InteractiveUtils

# ╔═╡ 760fd6f8-bbf6-11ec-1855-8f8549d4f94e
using Revise

# ╔═╡ 281b3ad2-d379-4f75-bc4e-20f2ff926bb8
using JuMP

# ╔═╡ 3ee367b2-a8d8-4a38-af14-2859487d79cc
using GLPK

# ╔═╡ 776add42-0874-4eef-bbe4-0bf1b9acf956
md"""
# 
### Тема: Оптимізація використання заготовлених кормів

#### Мета: Засвоїти теоретичний матеріал. Закріпити на практиці набуті знання

#### Виконав: Юрій Харченко

##### В роботі використано пакети мови Julia - Pluto, JuMP, GLPK
"""

# ╔═╡ b2bfb0cb-be91-4a76-a9ed-5706195ebb69
job = let

	# Групи тварин
	I = [
		"Дійні корови", 
		"Молодняк ВРХ",
	]
	# Норма кормових одиниць на 1 гол
	D = Dict(zip(I, [18.0 10.0]))
	# Норма перетравного прот на 1 гол
	E = Dict(zip(I, [187.0 110.0]))
	# Норма кормових одиниць на од. прод
	F = Dict(zip(I, [1.16 7.7]))
	# Норма перетравного прот на од. прод
	G = Dict(zip(I, [10.7 79.0]))
	# Норма поголів'я
	H = Dict(zip(I, [1600.0 2400.0]))
	# Ціна валової продукції в розрахунку на 1 голову
	C1 = Dict(zip(I, [495.0 251.0]))
	# Ціна реалізації молока/приросту
	C2 = Dict(zip(I, [21.0 137.0]))
	
	# Корми
	J = [
		"Комбікорм",
		"Зернові відходи",
		"Сіно",
		"Солома",
		"Силос",
		"Кормові буряки",
	]
	# Кормові одиниці
	A = Dict(zip(J, [0.92 0.66 0.45 0.2 0.14 0.12]))
	# Перетрав. протеїн
	B = Dict(zip(J, [14.0 8.2 6.6 1.0 1.2 0.9]))
	# Кількість заготовлених у господарстві кормів
	Q = Dict(zip(J, [9685.0 5856.0 30990.0 37093.0 102800.0 16440.0]))
	# Ціна купівлі/продажу кормів
	P = Dict(zip(J, [12.0 0.0 0.0 1.2 0.0 0.0]))
	# Ліміт коштів купівлі кормів
	L = 69000
	# Ліміти купівлі/продажу кормів
	Lb = Dict(zip(J, [6500.0 0.0 0.0 0.0 0.0 0.0]))
	Ls = Dict(zip(J, [0.0 0.0 0.0 37093.0 0.0 0.0]))
		
	# Концентровані
	J1 = [
		"Комбікорм",
		"Зернові відходи",
	]
	# Норма кормових одиниць на 1 гол
	D1min = Dict(zip(I, [2.7 2.0]))
	D1max = Dict(zip(I, [5.4 4.0]))
	# Норма кормових одиниць на од. прод
	F1min = Dict(zip(I, [0.17 1.54]))
	F1max = Dict(zip(I, [0.35 3.08]))
	
	# Грубі
	J2 = [
		"Сіно",
		"Солома",
	]
	# Норма кормових одиниць на 1 гол
	D2min = Dict(zip(I, [3.4 1.5]))
	D2max = Dict(zip(I, [7.2 3.5]))
	# Норма кормових одиниць на од. прод
	F2min = Dict(zip(I, [0.23 1.16]))
	F2max = Dict(zip(I, [0.46 2.7]))

	
	# Соковиті(силос)
	J3 = [
		"Силос",
	]
	# Норма кормових одиниць на 1 гол
	D3min = Dict(zip(I, [3.6 2.0]))
	D3max = Dict(zip(I, [6.3 3.5]))
	# Норма кормових одиниць на од. прод
	F3min = Dict(zip(I, [0.23 1.54]))
	F3max = Dict(zip(I, [0.41 2.7]))
	
	# Коренеплоди
	J4 = [
		"Кормові буряки",
	]
	# Норма кормових одиниць на 1 гол
	D4min = Dict(zip(I, [0.9 0.2]))
	D4max = Dict(zip(I, [1.8 0.5]))
	# Норма кормових одиниць на од. прод
	F4min = Dict(zip(I, [0.06 0.15]))
	F4max = Dict(zip(I, [0.12 0.39]))
		
	M = [
		"Придбання",
		"Продаж",
	]
				
	model = Model(GLPK.Optimizer)
	
	# Корми
	@variable(model, x[I, J] >= 0)
	# Поголів'я
	@variable(model, y[I] >= 0)
	# Приріст вир-ва
	@variable(model, z[I] >= 0)
	# Придбання, Реалізація
	@variable(model, v[M, J] >= 0)
		
	# Баланс кормових одиниць
	for i in I
		@constraint(model, 
			sum(A[j]*x[i, j] for j in J) >= D[i]*y[i]+F[i]*z[i])
	end

	# Баланс перетрав. протеїну
	for i in I
		@constraint(model, 
			sum(B[j]*x[i, j] for j in J) >= E[i]*y[i]+G[i]*z[i])
	end

	# Концентровані
	for i in I
		# мінімум
		@constraint(model, 
			sum(A[j]*x[i, j] for j in J1) >= D1min[i]*y[i]+F1min[i]*z[i])
		# максимум
		@constraint(model, 
			sum(A[j]*x[i, j] for j in J1) <= D1max[i]*y[i]+F1max[i]*z[i])
	end

	# Грубі
	for i in I
		# мінімум
		@constraint(model, 
			sum(A[j]*x[i, j] for j in J2) >= D2min[i]*y[i]+F2min[i]*z[i])
		# максимум
		@constraint(model, 
			sum(A[j]*x[i, j] for j in J2) <= D2max[i]*y[i]+F2max[i]*z[i])
	end

	# Соковиті(силос)
	for i in I
		# мінімум
		@constraint(model, 
			sum(A[j]*x[i, j] for j in J3) >= D3min[i]*y[i]+F3min[i]*z[i])
		# максимум
		@constraint(model, 
			sum(A[j]*x[i, j] for j in J3) <= D3max[i]*y[i]+F3max[i]*z[i])
	end

	# Коренеплоди
	for i in I
		# мінімум
		@constraint(model, 
			sum(A[j]*x[i, j] for j in J4) >= D4min[i]*y[i]+F4min[i]*z[i])
		# максимум
		@constraint(model, 
			sum(A[j]*x[i, j] for j in J4) <= D4max[i]*y[i]+F4max[i]*z[i])
	end

	# Поголів'я
	for i in I
		@constraint(model, 
			y[i] == H[i])
	end

	# Розподіл кормів 
	for j in J
		@constraint(model, 
			sum(x[i, j] for i in I) <= Q[j]+v["Придбання", j]-v["Продаж", j])
	end

	# Ліміти купівлі/продажу кормів
	for j in J
		@constraint(model, 
			v["Придбання", j] <= Lb[j])
		@constraint(model, 
			v["Продаж", j] <= Ls[j])
	end
	# Ліміти коштів
	@constraint(model, 
			sum(P[j]*v["Придбання", j] for j in J) <= L+sum(P[j]*v["Продаж", j] for j in J))

	# Функція мети – максимум виробництва тваринницької продукції, грош. од.
	@objective(model, Max,
			sum(C1[i]*y[i] for i in I)
			+sum(C2[i]*z[i] for i in I)
			-sum(P[j]*v["Придбання", j] for j in J)
			+sum(P[j]*v["Продаж", j] for j in J)
	)
		
	optimize!(model)
	result = solution_summary(model, verbose=true)

	out = md"""
# 
##### Висновок
	
Щоб задовольнити план використання кормів при заданих нормах та вимогах потрібно придбати $(round(value(v["Придбання","Комбікорм"]))) ц комбікорму та продати $(round(value(v["Продаж","Солома"]))) ц соломи.

Цільова функція прийме значення $(round(objective_value(model)/1000)) тис грош од
	"""

	(model = model, result = result,  out = out)
end;

# ╔═╡ e598f84f-dede-467f-820b-a0e1f1ad2754
job.model

# ╔═╡ 94551003-372c-4fdb-8fe0-27b4487908b1
job.result

# ╔═╡ 0ac91378-49df-4975-8cbe-ea73de834e95
job.out

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
GLPK = "60bf3e95-4087-53dc-ae20-288a0d20c6a6"
JuMP = "4076af6c-e467-56ae-b986-b466b2749572"
Revise = "295af30f-e4ad-537b-8983-00126c2a3abe"

[compat]
GLPK = "~1.0.1"
JuMP = "~1.0.0"
Revise = "~3.3.3"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.1"
manifest_format = "2.0"

[[deps.ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.BenchmarkTools]]
deps = ["JSON", "Logging", "Printf", "Profile", "Statistics", "UUIDs"]
git-tree-sha1 = "4c10eee4af024676200bc7752e536f858c6b8f93"
uuid = "6e4b80f9-dd63-53aa-95a3-0cdb28fa8baf"
version = "1.3.1"

[[deps.Bzip2_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "19a35467a82e236ff51bc17a3a44b69ef35185a2"
uuid = "6e34b625-4abd-537c-b88f-471c36dfa7a0"
version = "1.0.8+0"

[[deps.Calculus]]
deps = ["LinearAlgebra"]
git-tree-sha1 = "f641eb0a4f00c343bbc32346e1217b86f3ce9dad"
uuid = "49dc2e85-a5d0-5ad3-a950-438e2897f1b9"
version = "0.5.1"

[[deps.ChainRulesCore]]
deps = ["Compat", "LinearAlgebra", "SparseArrays"]
git-tree-sha1 = "9950387274246d08af38f6eef8cb5480862a435f"
uuid = "d360d2e6-b24c-11e9-a2a3-2a2ae2dbcce4"
version = "1.14.0"

[[deps.ChangesOfVariables]]
deps = ["ChainRulesCore", "LinearAlgebra", "Test"]
git-tree-sha1 = "bf98fa45a0a4cee295de98d4c1462be26345b9a1"
uuid = "9e997f8a-9a97-42d5-a9f1-ce6bfc15e2c0"
version = "0.1.2"

[[deps.CodeTracking]]
deps = ["InteractiveUtils", "UUIDs"]
git-tree-sha1 = "9fb640864691a0936f94f89150711c36072b0e8f"
uuid = "da1fd8a2-8d9e-5ec2-8556-3022fb5608a2"
version = "1.0.8"

[[deps.CodecBzip2]]
deps = ["Bzip2_jll", "Libdl", "TranscodingStreams"]
git-tree-sha1 = "2e62a725210ce3c3c2e1a3080190e7ca491f18d7"
uuid = "523fee87-0ab8-5b00-afb7-3ecf72e48cfd"
version = "0.7.2"

[[deps.CodecZlib]]
deps = ["TranscodingStreams", "Zlib_jll"]
git-tree-sha1 = "ded953804d019afa9a3f98981d99b33e3db7b6da"
uuid = "944b1d66-785c-5afd-91f1-9de20f533193"
version = "0.7.0"

[[deps.CommonSubexpressions]]
deps = ["MacroTools", "Test"]
git-tree-sha1 = "7b8a93dba8af7e3b42fecabf646260105ac373f7"
uuid = "bbf7d656-a473-5ed7-a52c-81e309532950"
version = "0.3.0"

[[deps.Compat]]
deps = ["Base64", "Dates", "DelimitedFiles", "Distributed", "InteractiveUtils", "LibGit2", "Libdl", "LinearAlgebra", "Markdown", "Mmap", "Pkg", "Printf", "REPL", "Random", "SHA", "Serialization", "SharedArrays", "Sockets", "SparseArrays", "Statistics", "Test", "UUIDs", "Unicode"]
git-tree-sha1 = "b153278a25dd42c65abbf4e62344f9d22e59191b"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "3.43.0"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[deps.DataStructures]]
deps = ["Compat", "InteractiveUtils", "OrderedCollections"]
git-tree-sha1 = "3daef5523dd2e769dad2365274f760ff5f282c7d"
uuid = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
version = "0.18.11"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.DelimitedFiles]]
deps = ["Mmap"]
uuid = "8bb1440f-4735-579b-a4ab-409b98df4dab"

[[deps.DiffResults]]
deps = ["StaticArrays"]
git-tree-sha1 = "c18e98cba888c6c25d1c3b048e4b3380ca956805"
uuid = "163ba53b-c6d8-5494-b064-1a9d43ac40c5"
version = "1.0.3"

[[deps.DiffRules]]
deps = ["IrrationalConstants", "LogExpFunctions", "NaNMath", "Random", "SpecialFunctions"]
git-tree-sha1 = "dd933c4ef7b4c270aacd4eb88fa64c147492acf0"
uuid = "b552c78f-8df3-52c6-915a-8e097449b14b"
version = "1.10.0"

[[deps.Distributed]]
deps = ["Random", "Serialization", "Sockets"]
uuid = "8ba89e20-285c-5b6f-9357-94700520ee1b"

[[deps.DocStringExtensions]]
deps = ["LibGit2"]
git-tree-sha1 = "b19534d1895d702889b219c382a6e18010797f0b"
uuid = "ffbed154-4ef7-542d-bbb7-c09d3a79fcae"
version = "0.8.6"

[[deps.Downloads]]
deps = ["ArgTools", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"

[[deps.FileWatching]]
uuid = "7b1f6079-737a-58dc-b8bc-7a2ca5c1b5ee"

[[deps.ForwardDiff]]
deps = ["CommonSubexpressions", "DiffResults", "DiffRules", "LinearAlgebra", "LogExpFunctions", "NaNMath", "Preferences", "Printf", "Random", "SpecialFunctions", "StaticArrays"]
git-tree-sha1 = "1bd6fc0c344fc0cbee1f42f8d2e7ec8253dda2d2"
uuid = "f6369f11-7733-5829-9624-2563aa707210"
version = "0.10.25"

[[deps.GLPK]]
deps = ["GLPK_jll", "MathOptInterface"]
git-tree-sha1 = "c3cc0a7a4e021620f1c0e67679acdbf1be311eb0"
uuid = "60bf3e95-4087-53dc-ae20-288a0d20c6a6"
version = "1.0.1"

[[deps.GLPK_jll]]
deps = ["Artifacts", "GMP_jll", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "fe68622f32828aa92275895fdb324a85894a5b1b"
uuid = "e8aa6df9-e6ca-548a-97ff-1f85fc5b8b98"
version = "5.0.1+0"

[[deps.GMP_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "781609d7-10c4-51f6-84f2-b8444358ff6d"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.InverseFunctions]]
deps = ["Test"]
git-tree-sha1 = "91b5dcf362c5add98049e6c29ee756910b03051d"
uuid = "3587e190-3f89-42d0-90ee-14403ec27112"
version = "0.1.3"

[[deps.IrrationalConstants]]
git-tree-sha1 = "7fd44fd4ff43fc60815f8e764c0f352b83c49151"
uuid = "92d709cd-6900-40b7-9082-c6be49f344b6"
version = "0.1.1"

[[deps.JLLWrappers]]
deps = ["Preferences"]
git-tree-sha1 = "abc9885a7ca2052a736a600f7fa66209f96506e1"
uuid = "692b3bcd-3c85-4b1f-b108-f13ce0eb3210"
version = "1.4.1"

[[deps.JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "3c837543ddb02250ef42f4738347454f95079d4e"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.3"

[[deps.JuMP]]
deps = ["Calculus", "DataStructures", "ForwardDiff", "LinearAlgebra", "MathOptInterface", "MutableArithmetics", "NaNMath", "OrderedCollections", "Printf", "SparseArrays", "SpecialFunctions"]
git-tree-sha1 = "936e7ebf6c84f0c0202b83bb22461f4ebc5c9969"
uuid = "4076af6c-e467-56ae-b986-b466b2749572"
version = "1.0.0"

[[deps.JuliaInterpreter]]
deps = ["CodeTracking", "InteractiveUtils", "Random", "UUIDs"]
git-tree-sha1 = "cd6ce9cee498f6b044357cb439f2a3fea3d450df"
uuid = "aa1ae85d-cabe-5617-a682-6adf51b2e16a"
version = "0.9.12"

[[deps.LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"

[[deps.LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"

[[deps.LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[deps.LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.LogExpFunctions]]
deps = ["ChainRulesCore", "ChangesOfVariables", "DocStringExtensions", "InverseFunctions", "IrrationalConstants", "LinearAlgebra"]
git-tree-sha1 = "a970d55c2ad8084ca317a4658ba6ce99b7523571"
uuid = "2ab3a3ac-af41-5b50-aa03-7779005ae688"
version = "0.3.12"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.LoweredCodeUtils]]
deps = ["JuliaInterpreter"]
git-tree-sha1 = "6b0440822974cab904c8b14d79743565140567f6"
uuid = "6f1432cf-f94c-5a45-995e-cdbf5db27b0b"
version = "2.2.1"

[[deps.MacroTools]]
deps = ["Markdown", "Random"]
git-tree-sha1 = "3d3e902b31198a27340d0bf00d6ac452866021cf"
uuid = "1914dd2f-81c6-5fcd-8719-6d5c9610ff09"
version = "0.5.9"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MathOptInterface]]
deps = ["BenchmarkTools", "CodecBzip2", "CodecZlib", "JSON", "LinearAlgebra", "MutableArithmetics", "OrderedCollections", "Printf", "SparseArrays", "Test", "Unicode"]
git-tree-sha1 = "779ad2ee78c4a24383887fdba177e9e5034ce207"
uuid = "b8f27783-ece8-5eb3-8dc8-9495eed66fee"
version = "1.1.2"

[[deps.MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"

[[deps.MutableArithmetics]]
deps = ["LinearAlgebra", "SparseArrays", "Test"]
git-tree-sha1 = "ba8c0f8732a24facba709388c74ba99dcbfdda1e"
uuid = "d8a4904e-b15c-11e9-3269-09a3773c0cb0"
version = "1.0.0"

[[deps.NaNMath]]
git-tree-sha1 = "b086b7ea07f8e38cf122f5016af580881ac914fe"
uuid = "77ba4419-2d1f-58cd-9bb1-8ffee604a2e3"
version = "0.3.7"

[[deps.NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[deps.OpenLibm_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "05823500-19ac-5b8b-9628-191a04bc5112"

[[deps.OpenSpecFun_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "13652491f6856acfd2db29360e1bbcd4565d04f1"
uuid = "efe28fd5-8261-553b-a9e1-b2916fc3738e"
version = "0.5.5+0"

[[deps.OrderedCollections]]
git-tree-sha1 = "85f8e6578bf1f9ee0d11e7bb1b1456435479d47c"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.4.1"

[[deps.Parsers]]
deps = ["Dates"]
git-tree-sha1 = "621f4f3b4977325b9128d5fae7a8b4829a0c2222"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "2.2.4"

[[deps.Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"

[[deps.Preferences]]
deps = ["TOML"]
git-tree-sha1 = "d3538e7f8a790dc8903519090857ef8e1283eecd"
uuid = "21216c6a-2e73-6563-6e65-726566657250"
version = "1.2.5"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.Profile]]
deps = ["Printf"]
uuid = "9abbd945-dff8-562f-b5e8-e1ebf5ef1b79"

[[deps.REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.Requires]]
deps = ["UUIDs"]
git-tree-sha1 = "838a3a4188e2ded87a4f9f184b4b0d78a1e91cb7"
uuid = "ae029012-a4dd-5104-9daa-d747884805df"
version = "1.3.0"

[[deps.Revise]]
deps = ["CodeTracking", "Distributed", "FileWatching", "JuliaInterpreter", "LibGit2", "LoweredCodeUtils", "OrderedCollections", "Pkg", "REPL", "Requires", "UUIDs", "Unicode"]
git-tree-sha1 = "4d4239e93531ac3e7ca7e339f15978d0b5149d03"
uuid = "295af30f-e4ad-537b-8983-00126c2a3abe"
version = "3.3.3"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.SharedArrays]]
deps = ["Distributed", "Mmap", "Random", "Serialization"]
uuid = "1a1011a3-84de-559e-8e89-a11a2f7dc383"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.SpecialFunctions]]
deps = ["ChainRulesCore", "IrrationalConstants", "LogExpFunctions", "OpenLibm_jll", "OpenSpecFun_jll"]
git-tree-sha1 = "5ba658aeecaaf96923dce0da9e703bd1fe7666f9"
uuid = "276daf66-3868-5448-9aa4-cd146d93841b"
version = "2.1.4"

[[deps.StaticArrays]]
deps = ["LinearAlgebra", "Random", "Statistics"]
git-tree-sha1 = "4f6ec5d99a28e1a749559ef7dd518663c5eca3d5"
uuid = "90137ffa-7385-5640-81b9-e52037218182"
version = "1.4.3"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"

[[deps.Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"

[[deps.Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[deps.TranscodingStreams]]
deps = ["Random", "Test"]
git-tree-sha1 = "216b95ea110b5972db65aa90f88d8d89dcb8851c"
uuid = "3bb67fe8-82b1-5028-8e26-92a6c54297fa"
version = "0.9.6"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"

[[deps.nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"

[[deps.p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
"""

# ╔═╡ Cell order:
# ╠═760fd6f8-bbf6-11ec-1855-8f8549d4f94e
# ╠═776add42-0874-4eef-bbe4-0bf1b9acf956
# ╠═281b3ad2-d379-4f75-bc4e-20f2ff926bb8
# ╠═3ee367b2-a8d8-4a38-af14-2859487d79cc
# ╠═b2bfb0cb-be91-4a76-a9ed-5706195ebb69
# ╠═e598f84f-dede-467f-820b-a0e1f1ad2754
# ╠═94551003-372c-4fdb-8fe0-27b4487908b1
# ╠═0ac91378-49df-4975-8cbe-ea73de834e95
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
