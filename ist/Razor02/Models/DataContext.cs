using Microsoft.EntityFrameworkCore;

namespace Razor02.Models
{
    public class DataContext : DbContext
    {
        public DbSet<Student> Students { get; set; }
        public DbSet<Enrollment> Enrollments { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DataContext(DbContextOptions<DataContext> options) :
        base(options)
        {
            Database.EnsureCreated();
        }
    }
}