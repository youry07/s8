namespace mvc01.Models
{
    public enum Category
    {
        Піца,
        Ланч,
        Суши
    }

    public class Product
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
        public Category Category { get; set; }
        public float Price { get; set; }
    }
}

