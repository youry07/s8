using Microsoft.EntityFrameworkCore;

namespace mvc01.Models
{
    public class DataContext : DbContext
    {
        public DbSet<Product> Products { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DataContext(DbContextOptions<DataContext> options) :
        base(options)
        {
            Database.EnsureCreated();
        }
    }
}