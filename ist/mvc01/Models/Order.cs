namespace mvc01.Models
{
    public class Order
    {
        public int OrderId { get; set; }
        public string Name { get; set; }
        public string Addres { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; }
    }
}