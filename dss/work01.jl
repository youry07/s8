### A Pluto.jl notebook ###
# v0.17.5

using Markdown
using InteractiveUtils

# ╔═╡ 026974f2-89a4-11ec-1973-f5579e75c550
using Revise

# ╔═╡ 77d6aca4-b37c-45f0-9b0d-79b6ae5720dd
using PlutoUI

# ╔═╡ f23529fa-5450-4a60-ad3c-c87bfc6937bf
using JuMP

# ╔═╡ 528f1c13-8a6d-4a8d-b547-fbf112ba8bc7
using GLPK

# ╔═╡ 88b45368-2f1b-4b60-b918-bb7c6764c3ba
TableOfContents(depth=4)

# ╔═╡ e17062a7-fd7a-4cdc-ab4d-646fc0d73f1b
md"""
# 
### Тема: Цільове програмування

#### Мета: Навчитися використовувати алгоритми цільового програмування для розв'язання багатокритеріальних задач економіко-математичного програмування.

#### Виконав: Юрій Харченко

##### В роботі використано пакети мови Julia - Pluto, JuMP, GLPK
"""

# ╔═╡ e7785811-0df2-4231-8c8a-de4799959fab
md"""
# 
#### Тестовий приклад по пакету JuMP
"""

# ╔═╡ a5a7abbf-930c-4d69-98ba-f154098c7b31
job00 = let
	model = Model(GLPK.Optimizer)
	
	problem = (
		@variable(model, x >= 0),
		@variable(model, 0 <= y <= 3),
		@objective(model, Min, 12x + 20y),
		@constraint(model, c1, 6x + 8y >= 100),
		@constraint(model, c2, 7x + 12y >= 120)
	)

	optimize!(model)
	result = solution_summary(model, verbose=true)

	Dict("problem" => problem, "model" => model, "result" => result)
	
end;

# ╔═╡ 12249dc6-3e84-4801-962d-ee5445b6295a
job00["problem"]

# ╔═╡ de2d3149-37d4-4877-a9ce-1f9c76a75afd
job00["model"]

# ╔═╡ 0ddbcac0-b50d-4537-a816-2422e69dcc1b
job00["result"]

# ╔═╡ 9b944cc4-a1c8-4eba-a9b8-d5db5d18b5ab
md"""
# 
#### Приклад Файрвілл (Таха)
"""

# ╔═╡ ae8b7b24-a907-4c7d-9964-b0544d1518e4
md"""
# 
##### Модифікована задача - min S₁⁺ + S₂⁺ + S₃⁺ + S₄⁻
"""

# ╔═╡ 1559f1a5-e2a8-4ddf-b102-acc83c997fb5
job01 = let
	
	model = Model(GLPK.Optimizer)
	
	problem = (
		@variable(model, xₙ >= 0),
		@variable(model, xᵣ >= 0),
		@variable(model, xₒ >= 0),
		@variable(model, xᵦ >= 0),
		@variable(model, S₁⁺ >= 0),
		@variable(model, S₁⁻ >= 0),
		@variable(model, S₂⁺ >= 0),
		@variable(model, S₂⁻ >= 0),
		@variable(model, S₃⁺ >= 0),
		@variable(model, S₃⁻ >= 0),
		@variable(model, S₄⁺ >= 0),
		@variable(model, S₄⁻ >= 0),
		@constraint(model, cf1, 550xₙ + 35xᵣ + 55xₒ + 0.075xᵦ >= 16),
		@constraint(model, cf2, 55xₙ - 31.5xᵣ + 5.5xₒ + 0.0075xᵦ >= 0),
		@constraint(model, cf3, 110xₙ + 7xᵣ - 44xₒ + 0.015xᵦ >= 0),
		@constraint(model, cf4, xᵦ <= 2),
		@objective(model, Min, S₁⁺ + S₂⁺ + S₃⁺ + S₄⁻)
	)

	optimize!(model)
	result = solution_summary(model, verbose=true)

	Dict("problem" => problem, "model" => model, "result" => result)
	
end;

# ╔═╡ 0738d3ea-9d80-42b6-9ebf-e29ce2eb0b3f
job01["problem"]

# ╔═╡ 25fc85de-0ba6-4f61-8046-032c13c0f20d
job01["model"]

# ╔═╡ 36b71f92-fbc3-4673-b97a-e4cce174f5d9
job01["result"]

# ╔═╡ 3417c645-d1e8-4e3a-9a9f-b667a51b4f1e
md"""
# 
##### Модифікована задача - min S₁⁺
"""

# ╔═╡ 811d3589-0038-4325-8165-147a162a876b
job02 = let
	
	model = Model(GLPK.Optimizer)
	
	problem = (
		@variable(model, xₙ >= 0),
		@variable(model, xᵣ >= 0),
		@variable(model, xₒ >= 0),
		@variable(model, xᵦ >= 0),
		@variable(model, S₁⁺ >= 0),
		@variable(model, S₁⁻ >= 0),
		@variable(model, S₂⁺ >= 0),
		@variable(model, S₂⁻ >= 0),
		@variable(model, S₃⁺ >= 0),
		@variable(model, S₃⁻ >= 0),
		@variable(model, S₄⁺ >= 0),
		@variable(model, S₄⁻ >= 0),
		@constraint(model, cf1, 550xₙ + 35xᵣ + 55xₒ + 0.075xᵦ + S₁⁺ - S₁⁻ == 16),
		@constraint(model, cf2, 55xₙ - 31.5xᵣ + 5.5xₒ + 0.0075xᵦ + S₂⁺ - S₂⁻ == 0),
		@constraint(model, cf3, 110xₙ + 7xᵣ - 44xₒ + 0.015xᵦ + S₃⁺ - S₃⁻ == 0),
		@constraint(model, cf4, xᵦ + S₄⁺ - S₄⁻ == 2),
		@objective(model, Min, S₁⁺)
	)

	optimize!(model)
	result = solution_summary(model, verbose=true)

	Dict("problem" => problem, "model" => model, "result" => result)
	
end;

# ╔═╡ 12724bdf-a1b7-415b-a51b-09591f38e8e4
job02["problem"]

# ╔═╡ 5de97ae3-539c-4f13-8a1f-da36db0c598b
job02["model"]

# ╔═╡ 2962d13d-1f0b-489c-a592-c19f9db14342
job02["result"]

# ╔═╡ 0095f925-c280-4f6b-be22-aabc2a251814
md"""
# 
##### Модифікована задача - min S₂⁺
"""

# ╔═╡ afeece53-ad04-42be-b330-e1fba9dbaf13
job03 = let
	
	model = Model(GLPK.Optimizer)
	
	problem = (
		@variable(model, xₙ >= 0),
		@variable(model, xᵣ >= 0),
		@variable(model, xₒ >= 0),
		@variable(model, xᵦ >= 0),
		@variable(model, S₁⁺ >= 0),
		@variable(model, S₁⁻ >= 0),
		@variable(model, S₂⁺ >= 0),
		@variable(model, S₂⁻ >= 0),
		@variable(model, S₃⁺ >= 0),
		@variable(model, S₃⁻ >= 0),
		@variable(model, S₄⁺ >= 0),
		@variable(model, S₄⁻ >= 0),
		@constraint(model, cf1, 550xₙ + 35xᵣ + 55xₒ + 0.075xᵦ + S₁⁺ - S₁⁻ == 16),
		@constraint(model, cf2, 55xₙ - 31.5xᵣ + 5.5xₒ + 0.0075xᵦ + S₂⁺ - S₂⁻ == 0),
		@constraint(model, cf3, 110xₙ + 7xᵣ - 44xₒ + 0.015xᵦ + S₃⁺ - S₃⁻ == 0),
		@constraint(model, cf4, xᵦ + S₄⁺ - S₄⁻ == 2),
		@objective(model, Min, S₂⁺)
	)

	optimize!(model)
	result = solution_summary(model, verbose=true)

	Dict("problem" => problem, "model" => model, "result" => result)
	
end;

# ╔═╡ 4b6d45fd-7247-4ede-a96f-908a70488282
job03["result"]

# ╔═╡ f627ab7f-62e2-4403-929b-58c647d0bc92
md"""
# 
##### Модифікована задача - min S₃⁺
"""

# ╔═╡ 50c5bc9c-88f9-45b0-bb4c-a4a5156d995e
job04 = let
	
	model = Model(GLPK.Optimizer)
	
	problem = (
		@variable(model, xₙ >= 0),
		@variable(model, xᵣ >= 0),
		@variable(model, xₒ >= 0),
		@variable(model, xᵦ >= 0),
		@variable(model, S₁⁺ >= 0),
		@variable(model, S₁⁻ >= 0),
		@variable(model, S₂⁺ >= 0),
		@variable(model, S₂⁻ >= 0),
		@variable(model, S₃⁺ >= 0),
		@variable(model, S₃⁻ >= 0),
		@variable(model, S₄⁺ >= 0),
		@variable(model, S₄⁻ >= 0),
		@constraint(model, cf1, 550xₙ + 35xᵣ + 55xₒ + 0.075xᵦ + S₁⁺ - S₁⁻ == 16),
		@constraint(model, cf2, 55xₙ - 31.5xᵣ + 5.5xₒ + 0.0075xᵦ + S₂⁺ - S₂⁻ == 0),
		@constraint(model, cf3, 110xₙ + 7xᵣ - 44xₒ + 0.015xᵦ + S₃⁺ - S₃⁻ == 0),
		@constraint(model, cf4, xᵦ + S₄⁺ - S₄⁻ == 2),
		@objective(model, Min, S₃⁺)
	)

	optimize!(model)
	result = solution_summary(model, verbose=true)

	Dict("problem" => problem, "model" => model, "result" => result)
	
end;

# ╔═╡ 871b936e-1e5c-4e09-bb0e-fe57f75a4a68
job04["result"]

# ╔═╡ 66dc45aa-9f86-47b1-a581-657c5df275a2
md"""
# 
##### Модифікована задача - min S₄⁻
"""

# ╔═╡ 1f675f9f-89b5-4e80-ba5d-cc16057a6212
job05 = let
	
	model = Model(GLPK.Optimizer)
	
	problem = (
		@variable(model, xₙ >= 0),
		@variable(model, xᵣ >= 0),
		@variable(model, xₒ >= 0),
		@variable(model, xᵦ >= 0),
		@variable(model, S₁⁺ >= 0),
		@variable(model, S₁⁻ >= 0),
		@variable(model, S₂⁺ >= 0),
		@variable(model, S₂⁻ >= 0),
		@variable(model, S₃⁺ >= 0),
		@variable(model, S₃⁻ >= 0),
		@variable(model, S₄⁺ >= 0),
		@variable(model, S₄⁻ >= 0),
		@constraint(model, cf1, 550xₙ + 35xᵣ + 55xₒ + 0.075xᵦ + S₁⁺ - S₁⁻ == 16),
		@constraint(model, cf2, 55xₙ - 31.5xᵣ + 5.5xₒ + 0.0075xᵦ + S₂⁺ - S₂⁻ == 0),
		@constraint(model, cf3, 110xₙ + 7xᵣ - 44xₒ + 0.015xᵦ + S₃⁺ - S₃⁻ == 0),
		@constraint(model, cf4, xᵦ + S₄⁺ - S₄⁻ == 2),
		@objective(model, Min, S₄⁻)
	)

	optimize!(model)
	result = solution_summary(model, verbose=true)

	Dict("problem" => problem, "model" => model, "result" => result)
	
end;

# ╔═╡ a0cb8700-3f6b-41e1-8fdf-524695d7a0cf
job05["result"]

# ╔═╡ 0550d0c8-0678-43c3-95b1-176c64d0d41c
md"""
# 
##### Висновок

Проведені розрахунки показують, що критичним обмеженням є 4-е обмеження (ціна на бензин)

Ставки податків:
* оптова торгівля: 5.8%
* роздрібна торгівля: 4.6%
* нерухомість: 2.0%
"""

# ╔═╡ 6072a52a-a8ac-4658-8e24-1a6710f81d0f
md"""
# 
#### Приклад Рекламне агенство (Таха)
#### Вагові коефіціенти
"""

# ╔═╡ aa497cc9-b097-4789-bddb-4ac39f90f7f7
job06 = let
	
	model = Model(GLPK.Optimizer)
	
	problem = (
		@variable(model, x₁ >= 0),
		@variable(model, x₂ >= 0),
		@variable(model, S₁⁺ >= 0),
		@variable(model, S₁⁻ >= 0),
		@variable(model, S₂⁺ >= 0),
		@variable(model, S₂⁻ >= 0),
		@constraint(model, cf1, 4x₁ + 8x₂ + S₁⁺ - S₁⁻ == 45),
		@constraint(model, cf2, 8x₁ + 24x₂ + S₂⁺ - S₂⁻ == 100),
		@constraint(model, cf3, x₁ + 2x₂ <= 10),
		@constraint(model, cf4, x₁ <= 6),
		@objective(model, Min, 2S₁⁺ + S₂⁻)
	)

	optimize!(model)
	result = solution_summary(model, verbose=true)

	Dict("problem" => problem, "model" => model, "result" => result)
	
end;

# ╔═╡ 7a8c8c7a-d74b-4aaa-a0c3-31826a4ba386
job06["result"]

# ╔═╡ 78059c2c-28d8-4a5b-9f56-bbf3e6a987b3
md"""
# 
##### Висновок

Проведені розрахунки показують, що умови по бюджету виконуються (S₂⁻ : 0.0) при зменшенні аудиторії на 5 млн (S₁⁺ : 4.99...)
"""

# ╔═╡ cda93622-2a0b-4018-bbc9-272f79637ad5
md"""
# 
#### Приклад Рекламне агенство (Таха)
#### Метод приоритетів
"""

# ╔═╡ ed86d971-d772-4d56-ad27-781f2423dcef
md"""
# 
##### Етап 1
"""

# ╔═╡ 10eec671-6235-45b2-b2cd-2365193a554d
job07 = let
	
	model = Model(GLPK.Optimizer)
	
	problem = (
		@variable(model, x₁ >= 0),
		@variable(model, x₂ >= 0),
		@variable(model, S₁⁺ >= 0),
		@variable(model, S₁⁻ >= 0),
		@variable(model, S₂⁺ >= 0),
		@variable(model, S₂⁻ >= 0),
		@constraint(model, cf1, 4x₁ + 8x₂ + S₁⁺ - S₁⁻ == 45),
		@constraint(model, cf2, 8x₁ + 24x₂ + S₂⁺ - S₂⁻ == 100),
		@constraint(model, cf3, x₁ + 2x₂ <= 10),
		@constraint(model, cf4, x₁ <= 6),
		@objective(model, Min, S₁⁺)
	)

	optimize!(model)
	result = solution_summary(model, verbose=true)

	Dict("problem" => problem, "model" => model, "result" => result)
	
end;

# ╔═╡ 703caa00-c67b-4628-b0f4-a684fa2a3a98
job07["result"]

# ╔═╡ 3f89a4b7-d9d9-439d-847e-a37159ca0de1
md"""
# 
##### Етап 2
"""

# ╔═╡ 5e4269bf-eddc-44a5-975b-0f753b8500f8
job08 = let
	
	model = Model(GLPK.Optimizer)
	
	problem = (
		@variable(model, x₁ >= 0),
		@variable(model, x₂ >= 0),
		@variable(model, S₁⁺ == 5),
		@variable(model, S₁⁻ >= 0),
		@variable(model, S₂⁺ >= 0),
		@variable(model, S₂⁻ >= 0),
		@constraint(model, cf1, 4x₁ + 8x₂ + S₁⁺ - S₁⁻ == 45),
		@constraint(model, cf2, 8x₁ + 24x₂ + S₂⁺ - S₂⁻ == 100),
		@constraint(model, cf3, x₁ + 2x₂ <= 10),
		@constraint(model, cf4, x₁ <= 6),
		@objective(model, Min, S₂⁻)
	)

	optimize!(model)
	result = solution_summary(model, verbose=true)

	Dict("problem" => problem, "model" => model, "result" => result)
	
end;

# ╔═╡ 93610e18-df31-4e7b-bf01-e3bf3fea32ca
job08["result"]

# ╔═╡ 71b0fabc-a10c-4fe9-849c-30fa4a81d1d8
md"""
# 
##### Висновок

Результати збігаються з рішенням за методом вагових коефіціентів
"""

# ╔═╡ a8f9110a-faf8-42d1-8e71-c83666b2d267
md"""
# 
#### Задача 2

Університет Озарк проводить прийом студентів. Всіх вступників
можна розбити на три категорії: жителі даного штату, приїжджі з інших
штатів і закордоння студенти. Співвідношення між чоловіками і жінками
серед вступників даного штату та інших штатів рівні відповідно 1:1 і 3:2.
Для "міжнародних" студентів це співвідношення становить 8:1. При
зарахуванні студентів на перший курс одним з основних факторів, що
впливають на зарахування, є середній бал АСТ (Аmerican Со11еgе Теst -
Американський тест для вступників до коледжу). Статистика, накопичена
університетом, свідчить, що середній бал АСТ дорівнює 27, 26 і 23
відповідно для вступників даного штату, інших штатів та інших країн.
Приймальна комісія університету при прийомі першокурсників бажає
домогтися наступного.

a) На перший курс бажано прийняти не менше 1200 студентів.

b) Середній бал АСТ всіх першокурсників повинен бути не нижче 26.

c) Студенти-неамериканці повинні становити не менше 20% від усієї
кількості першокурсників.

d) Відношення кількості жінок і чоловіків бажано не менше 3:4.

е) Серед усіх прийнятих на перший курс жителі інших штатів
повинні складати не менше 20%.

Сформулюйте дану проблему як модель цільового програмування.

"""

# ╔═╡ 53ef74a7-2272-439f-b41e-1fdc20de106b
job09 = let
	
	model = Model(GLPK.Optimizer)
	
	problem = (
		@variable(model, x₁ >= 0),
		@variable(model, x₂ >= 0),
		@variable(model, x₃ >= 0),
		@variable(model, z₁ >= 0),
		@variable(model, z₂ >= 0),
		@variable(model, z₃ >= 0),
		@variable(model, S₁⁺ >= 0),
		@variable(model, S₁⁻ >= 0),
		@variable(model, S₂⁺ >= 0),
		@variable(model, S₂⁻ >= 0),
		@variable(model, S₃⁺ >= 0),
		@variable(model, S₃⁻ >= 0),
		@variable(model, S₄⁺ >= 0),
		@variable(model, S₄⁻ >= 0),
		@variable(model, S₅⁺ >= 0),
		@variable(model, S₅⁻ >= 0),
		@constraint(model, cf0, x₁ + x₂ + x₃ == z₁),
		@constraint(model, cf1, z₁ + S₁⁺ - S₁⁻ == 1200),
		@constraint(model, cf2, 27x₁ + 26x₂ + 23x₃ + S₂⁺ - S₂⁻ == 26z₁),
		@constraint(model, cf3, x₃ + S₃⁺ - S₃⁻ == 0.2z₁ ),
		@constraint(model, cf4, (1/2)x₁ + (3/5)x₂ + (8/9)x₃ == z₂),
		@constraint(model, cf5, z₁ - z₂ == z₃),
		@constraint(model, cf6, 4z₃ + S₄⁺ - S₄⁻ == 3z₂),
		@constraint(model, cf7, x₂ + S₅⁺ - S₅⁻ == 0.2z₁),
		@objective(model, Min, S₁⁺)
	)

	optimize!(model)
	result = solution_summary(model, verbose=true)
	
	out = md"""
# 
##### Висновок
###### В результаті цільового програмування отримано такі результати
- з даного штату: $(round(value(x₁))) : $(100*round(value(x₁))/round(value(z₁)))%
- з інших штатів: $(round(value(x₂))) : $(100*round(value(x₂))/round(value(z₁)))%
- закордонні: $(round(value(x₃))) : $(100*round(value(x₃))/round(value(z₁)))%
- загалом: $(round(value(z₁)))
###### обмеження a) c) e) виконуються

- середній бал: $(round((value(x₁)*27+value(x₂)*26+value(x₃)*23)/value(z₁)))
###### обмеження b) виконується
			
- жінок: $(round(value(z₃))) : $(100*round(value(z₃))/round(value(z₁)))%
- чоловіків: $(round(value(z₂))) : $(100*round(value(z₂))/round(value(z₁)))%
- відношення: потрібно ≥ $(round(3/4, digits=2)), отримано  $(round(value(z₃)/value(z₂), digits=2))
###### рішення отримано за рахунок послаблення обмеження d)
"""

	Dict("problem" => problem, "model" => model, "result" => result, "out" => out)
	
end;

# ╔═╡ 00d13868-dbc9-40db-9820-ea9654ac21db
job09["result"]

# ╔═╡ 6af01c1b-fc1e-45b5-8c9e-0e788e712f3f
job09["out"]

# ╔═╡ b34a3226-aa17-4d1e-8b47-58c0e0f41d67
md"""
# 
#### Задача 4

Фабрика іграшок виробляє дитячі візки, для яких на стадії
кінцевої збірки необхідні чотири колеса і два сидіння. На фабриці робота
організована у три зміни, причому протягом однієї робочої зміни
виготовляється кілька партій коліс і сидінь. У наступній таблиці показані
обсяги партій виробів залежно від робочої зміни.

| Зміна | Колеса | Сидіння |
|:-----:|--------|---------|
| 1     | 500    | 300     |
| 2     | 600    | 280     |
| 3     | 640    | 360     |

В ідеалі кількість вироблених коліс повинно точно в два рази
перевищувати кількість вироблених сидінь (нагадаємо, що на кожний
візок йде чотири колеса і два сидіння). Але так як число вироблених
виробів коливається від зміни до зміни, неможливо забезпечити точний
баланс між кількостями вироблених коліс і сидінь. Фабрика планує
визначити, яка кількість партій виробів необхідно виготовляти кожну
зміну, щоб звести до мінімуму дисбаланс між виробленими колесами і
сидіннями. У першу зміну можна провести 4 або 5 партій виробів, у другу:
 від 10 до 20 партій, а в третю: від 3 до 5. Сформулюйте завдання
цільового програмування.

"""

# ╔═╡ f36529b3-8f80-40ee-a5e3-37f3ef07e268
job10 = let
	
	model = Model(GLPK.Optimizer)
	
	problem = (
		@variable(model, 4 <= x₁ <= 5),
		@variable(model, 10 <= x₂ <= 20),
		@variable(model, 3 <= x₃ <= 5),
		@variable(model, z₁ >= 0),
		@variable(model, z₂ >= 0),
		@variable(model, z₃ >= 0),
		@variable(model, S₁⁺ >= 0),
		@variable(model, S₁⁻ >= 0),
		@constraint(model, cf0, x₁ + x₂ + x₃ == z₁),
		@constraint(model, cf1, 500x₁ + 600x₂ + 640x₃ == z₂),
		@constraint(model, cf2, 300x₁ + 280x₂ + 360x₃ == z₃),
		@constraint(model, cf3, z₂ + S₁⁺ - S₁⁻ == 2z₃),
		@objective(model, Min, S₁⁺)
	)

	optimize!(model)
	result = solution_summary(model, verbose=true)
	
	out = md"""
# 
##### Висновок
###### В результаті цільового програмування отримано такі результати
		
Виготовлено партій:
- 1 зміна: $(round(value(x₁)))
- 2 зміна: $(round(value(x₂)))
- 3 зміна: $(round(value(x₃)))
- загалом: $(round(value(z₁)))
		
- колеса: $(round(value(z₂))) 
- сидіння: $(round(value(z₃)))
- відношення: потрібно $(round(1/2, digits=2)), отримано  $(round(value(z₃)/value(z₂), digits=2))
###### рішення отримано 
"""

	Dict("problem" => problem, "model" => model, "result" => result, "out" => out)
	
end;

# ╔═╡ 93ce5f3e-21e5-4152-8a2f-5f2541616237
job10["result"]

# ╔═╡ c842f225-69c0-4d8e-a93a-823826da08b0
job10["out"]

# ╔═╡ c98a7b82-43cb-4738-a8af-01743745d0d2
md"""
# 
#### Задача 6

Міська лікарня планує організувати для хворих короткочасний (до
4 днів) стаціонар на вільних місцях. Передбачається, що протягом 4-
денного періоду буде 30, 25 і 20 хворих, яким потрібно 1-, 2- і 3 денний
стаціонар. Також розраховано, що на той же період буде вільно 20, 30, 30 і
30 місць відповідно. Використовуйте цільове програмування, щоб
обчислити максимальну кількість хворих, яких можна прийняти на
короткочасне лікування.
"""

# ╔═╡ fc45566a-cdca-4ab2-96bd-d59d9f95b008
job11 = let
	
	model = Model(GLPK.Optimizer)
	
	problem = (
		@variable(model, 0 <= x₁¹ <= 30),
		@variable(model, 0 <= x₁² <= 25),
		@variable(model, 0 <= x₁³ <= 20),
		@variable(model, 0 <= x₂¹ <= 30),
		@variable(model, 0 <= x₂² <= 25),
		@variable(model, 0 <= x₂³ <= 20),
		@variable(model, 0 <= x₃¹ <= 30),
		@variable(model, 0 <= x₃² <= 25),
		@variable(model, 0 <= x₄¹ <= 30),
		@variable(model, 0 <= z₁ <= 20),
		@variable(model, 0 <= z₂ <= 30),
		@variable(model, 0 <= z₃ <= 30),
		@variable(model, 0 <= z₄ <= 30),
		@variable(model, S₁⁺ >= 0),
		@variable(model, S₁⁻ >= 0),
		@variable(model, S₂⁺ >= 0),
		@variable(model, S₂⁻ >= 0),
		@variable(model, S₃⁺ >= 0),
		@variable(model, S₃⁻ >= 0),
		@constraint(model, cf1, x₁¹ + x₁² + x₁³ == z₁),
		@constraint(model, cf2, x₁² + x₁³ + x₂¹ + x₂² + x₂³ == z₂),
		@constraint(model, cf3, x₁³ + x₂² + x₂³ + x₃¹ + x₃² == z₃),
		@constraint(model, cf4, x₂³ + x₃² + x₄¹ == z₄),
		@constraint(model, cf5, x₁¹ + x₂¹ + x₃¹ + x₄¹ + S₁⁺ - S₁⁻ == 30),
		@constraint(model, cf6, x₁² + x₂² + x₃² + S₂⁺ - S₂⁻ == 25),
		@constraint(model, cf7, x₁³ + x₂³ + S₃⁺ - S₃⁻ == 20),
				
		@objective(model, Min, S₁⁺ + S₂⁺ + S₃⁺)
	)

	optimize!(model)
	result = solution_summary(model, verbose=true)
	
	out = md"""
# 
##### Висновок
###### В результаті цільового програмування отримано такі результати
		
Поселено в 1-й день:
- на 1 день: $(round(value(x₁¹)))
- на 2 дні: $(round(value(x₁²)))
- на 3 дні: $(round(value(x₁³)))
заповненено: $(round(value(z₁)))

Поселено в 2-й день:
- на 1 день: $(round(value(x₂¹)))
- на 2 дні: $(round(value(x₂²)))
- на 3 дні: $(round(value(x₂³)))
заповненено: $(round(value(z₂)))

Поселено в 3-й день:
- на 1 день: $(round(value(x₃¹)))
- на 2 дні: $(round(value(x₃²)))
заповненено: $(round(value(z₃)))

Поселено в 4-й день:
- на 1 день: $(round(value(x₄¹)))
заповненено: $(round(value(z₄)))

###### рішення отримано за рахунок не надання послуг $(round(value(S₃⁺))) пацієнтам, які потребують 3-денного стаціонару
"""
		Dict("problem" => problem, "model" => model, "result" => result, "out" => out)
end;

# ╔═╡ 5fe8c2ac-8aec-4815-9487-d89edb88d006
job11["result"]

# ╔═╡ 74e33699-7b97-45c2-84e9-107eae7c8132
job11["out"]

# ╔═╡ 882abc98-e7ed-465f-8b05-b8ce99ccbfc5
md"""
# 
#### Задача 9

Університет Озарк проводить прийом студентів. Всіх вступників
можна розбити на три категорії: жителі даного штату, приїжджі з інших
штатів і закордоння студенти. Співвідношення між чоловіками і жінками
серед вступників даного штату та інших штатів рівні відповідно 1:1 і 3:2.
Для "міжнародних" студентів це співвідношення становить 8:1. При
зарахуванні студентів на перший курс одним з основних факторів, що
впливають на зарахування, є середній бал АСТ (Аmerican Со11еgе Теst -
Американський тест для вступників до коледжу). Статистика, накопичена
університетом, свідчить, що середній бал АСТ дорівнює 27, 26 і 23
відповідно для вступників даного штату, інших штатів та інших країн.
Приймальна комісія університету при прийомі першокурсників бажає
домогтися наступного.

a) На перший курс бажано прийняти не менше 1200 студентів.

b) Середній бал АСТ всіх першокурсників повинен бути не нижче 26.

c) Студенти-неамериканці повинні становити не менше 20% від усієї
кількості першокурсників.

d) Відношення кількості жінок і чоловіків бажано не менше 3:4.

е) Серед усіх прийнятих на перший курс жителі інших штатів
повинні складати не менше 20%.

Вважаємо, що обмеження на середній бал АСТ в
два рази важливіше, ніж інші обмеження.

"""

# ╔═╡ d7fb97d6-5337-43ec-aa47-e84dcd0c971e
job12 = let
	
	model = Model(GLPK.Optimizer)
	
	problem = (
		@variable(model, x₁ >= 0),
		@variable(model, x₂ >= 0),
		@variable(model, x₃ >= 0),
		@variable(model, z₁ >= 0),
		@variable(model, z₂ >= 0),
		@variable(model, z₃ >= 0),
		@variable(model, S₁⁺ >= 0),
		@variable(model, S₁⁻ >= 0),
		@variable(model, S₂⁺ >= 0),
		@variable(model, S₂⁻ >= 0),
		@variable(model, S₃⁺ >= 0),
		@variable(model, S₃⁻ >= 0),
		@variable(model, S₄⁺ >= 0),
		@variable(model, S₄⁻ >= 0),
		@variable(model, S₅⁺ >= 0),
		@variable(model, S₅⁻ >= 0),
		@constraint(model, cf0, x₁ + x₂ + x₃ == z₁),
		@constraint(model, cf1, z₁ + S₁⁺ - S₁⁻ == 1200),
		@constraint(model, cf2, 27x₁ + 26x₂ + 23x₃ + S₂⁺ - S₂⁻ == 26z₁),
		@constraint(model, cf3, x₃ + S₃⁺ - S₃⁻ == 0.2z₁ ),
		@constraint(model, cf4, (1/2)x₁ + (3/5)x₂ + (8/9)x₃ == z₂),
		@constraint(model, cf5, z₁ - z₂ == z₃),
		@constraint(model, cf6, 4z₃ + S₄⁺ - S₄⁻ == 3z₂),
		@constraint(model, cf7, x₂ + S₅⁺ - S₅⁻ == 0.2z₁),
		@objective(model, Min, S₁⁺ + 2S₂⁺)
	)

	optimize!(model)
	result = solution_summary(model, verbose=true)
	
	out = md"""
# 
##### Висновок
###### В результаті цільового програмування отримано такі результати
- з даного штату: $(round(value(x₁))) : $(100*round(value(x₁))/round(value(z₁)))%
- з інших штатів: $(round(value(x₂))) : $(100*round(value(x₂))/round(value(z₁)))%
- закордонні: $(round(value(x₃))) : $(100*round(value(x₃))/round(value(z₁)))%
- загалом: $(round(value(z₁)))
###### обмеження a) c) e) виконуються

- середній бал: $(round((value(x₁)*27+value(x₂)*26+value(x₃)*23)/value(z₁)))
###### обмеження b) виконується
			
- жінок: $(round(value(z₃))) : $(100*round(value(z₃))/round(value(z₁)))%
- чоловіків: $(round(value(z₂))) : $(100*round(value(z₂))/round(value(z₁)))%
- відношення: потрібно ≥ $(round(3/4, digits=2)), отримано  $(round(value(z₃)/value(z₂), digits=2))
###### рішення аналогічне задачі 2
"""

	Dict("problem" => problem, "model" => model, "result" => result, "out" => out)
	
end;

# ╔═╡ 5a634ca6-1280-447d-a8fd-dd800a75b3c2
job12["result"]

# ╔═╡ 0e75c336-4a51-4b21-a713-0f91e58c6b66
job12["out"]

# ╔═╡ a9cd7f50-913f-48cd-a7e2-2009deb08121
md"""
# 
#### Задача 12

Університет Озарк проводить прийом студентів. Всіх вступників
можна розбити на три категорії: жителі даного штату, приїжджі з інших
штатів і закордоння студенти. Співвідношення між чоловіками і жінками
серед вступників даного штату та інших штатів рівні відповідно 1:1 і 3:2.
Для "міжнародних" студентів це співвідношення становить 8:1. При
зарахуванні студентів на перший курс одним з основних факторів, що
впливають на зарахування, є середній бал АСТ (Аmerican Со11еgе Теst -
Американський тест для вступників до коледжу). Статистика, накопичена
університетом, свідчить, що середній бал АСТ дорівнює 27, 26 і 23
відповідно для вступників даного штату, інших штатів та інших країн.
Приймальна комісія університету при прийомі першокурсників бажає
домогтися наступного.

a) На перший курс бажано прийняти не менше 1200 студентів.

b) Середній бал АСТ всіх першокурсників повинен бути не нижче 26.

c) Студенти-неамериканці повинні становити не менше 20% від усієї
кількості першокурсників.

d) Відношення кількості жінок і чоловіків бажано не менше 3:4.

е) Серед усіх прийнятих на перший курс жителі інших штатів
повинні складати не менше 20%.

порядок пріоритетів часткових цільових функцій
збігається з порядком їх опису в задачі, а перше обмеження буде жорстким

"""

# ╔═╡ f9dc9abc-597c-4292-88ac-e6c8db3affd3
job13 = let
	
	model = Model(GLPK.Optimizer)
	
	problem = (
		@variable(model, x₁ >= 0),
		@variable(model, x₂ >= 0),
		@variable(model, x₃ >= 0),
		@variable(model, z₁ >= 0),
		@variable(model, z₂ >= 0),
		@variable(model, z₃ >= 0),
		@variable(model, S₁⁺ == 0),
		@variable(model, S₁⁻ == 0),
		@variable(model, S₂⁺ == 0),
		@variable(model, S₂⁻ == 0),
		@variable(model, S₃⁺ == 0),
		@variable(model, S₃⁻ == 0),
		@variable(model, S₄⁺ >= 0),
		@variable(model, S₄⁻ >= 0),
		@variable(model, S₅⁺ >= 0),
		@variable(model, S₅⁻ >= 0),
		@constraint(model, cf0, x₁ + x₂ + x₃ == z₁),
		@constraint(model, cf1, z₁ == 1200),
		@constraint(model, cf2, 27x₁ + 26x₂ + 23x₃ + S₂⁺ - S₂⁻ == 26z₁),
		@constraint(model, cf3, x₃ + S₃⁺ - S₃⁻ == 0.2z₁ ),
		@constraint(model, cf4, (1/2)x₁ + (3/5)x₂ + (8/9)x₃ == z₂),
		@constraint(model, cf5, z₁ - z₂ == z₃),
		@constraint(model, cf6, 4z₃ + S₄⁺ - S₄⁻ == 3z₂),
		@constraint(model, cf7, x₂ + S₅⁺ - S₅⁻ == 0.2z₁),
		@objective(model, Min, S₄⁺ )
	)

	optimize!(model)
	result = solution_summary(model, verbose=true)
	
	out = md"""
# 
##### Висновок
###### В результаті цільового програмування отримано такі результати
- з даного штату: $(round(value(x₁))) : $(100*round(value(x₁))/round(value(z₁)))%
- з інших штатів: $(round(value(x₂))) : $(100*round(value(x₂))/round(value(z₁)))%
- закордонні: $(round(value(x₃))) : $(100*round(value(x₃))/round(value(z₁)))%
- загалом: $(round(value(z₁)))
###### обмеження a) c) e) виконуються

- середній бал: $(round((value(x₁)*27+value(x₂)*26+value(x₃)*23)/value(z₁)))
###### обмеження b) виконується
			
- жінок: $(round(value(z₃))) : $(100*round(value(z₃))/round(value(z₁)))%
- чоловіків: $(round(value(z₂))) : $(100*round(value(z₂))/round(value(z₁)))%
- відношення: потрібно ≥ $(round(3/4, digits=2)), отримано  $(round(value(z₃)/value(z₂), digits=2))
###### рішення аналогічне задачі 2
"""

	Dict("problem" => problem, "model" => model, "result" => result, "out" => out)
	
end;

# ╔═╡ 3d4bf03e-2533-449e-8b58-b70ef5f343cf
job13["result"]

# ╔═╡ 05e4bd94-fa57-49b3-85f3-dc4980cb66b6
job13["out"]

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
GLPK = "60bf3e95-4087-53dc-ae20-288a0d20c6a6"
JuMP = "4076af6c-e467-56ae-b986-b466b2749572"
PlutoUI = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
Revise = "295af30f-e4ad-537b-8983-00126c2a3abe"

[compat]
GLPK = "~0.15.3"
JuMP = "~0.22.2"
PlutoUI = "~0.7.34"
Revise = "~3.3.1"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.1"
manifest_format = "2.0"

[[deps.AbstractPlutoDingetjes]]
deps = ["Pkg"]
git-tree-sha1 = "8eaf9f1b4921132a4cff3f36a1d9ba923b14a481"
uuid = "6e696c72-6542-2067-7265-42206c756150"
version = "1.1.4"

[[deps.ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.BenchmarkTools]]
deps = ["JSON", "Logging", "Printf", "Profile", "Statistics", "UUIDs"]
git-tree-sha1 = "be0cff14ad0059c1da5a017d66f763e6a637de6a"
uuid = "6e4b80f9-dd63-53aa-95a3-0cdb28fa8baf"
version = "1.3.0"

[[deps.BinaryProvider]]
deps = ["Libdl", "Logging", "SHA"]
git-tree-sha1 = "ecdec412a9abc8db54c0efc5548c64dfce072058"
uuid = "b99e7846-7c00-51b0-8f62-c81ae34c0232"
version = "0.5.10"

[[deps.Bzip2_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "19a35467a82e236ff51bc17a3a44b69ef35185a2"
uuid = "6e34b625-4abd-537c-b88f-471c36dfa7a0"
version = "1.0.8+0"

[[deps.CEnum]]
git-tree-sha1 = "215a9aa4a1f23fbd05b92769fdd62559488d70e9"
uuid = "fa961155-64e5-5f13-b03f-caf6b980ea82"
version = "0.4.1"

[[deps.Calculus]]
deps = ["LinearAlgebra"]
git-tree-sha1 = "f641eb0a4f00c343bbc32346e1217b86f3ce9dad"
uuid = "49dc2e85-a5d0-5ad3-a950-438e2897f1b9"
version = "0.5.1"

[[deps.ChainRulesCore]]
deps = ["Compat", "LinearAlgebra", "SparseArrays"]
git-tree-sha1 = "f9982ef575e19b0e5c7a98c6e75ee496c0f73a93"
uuid = "d360d2e6-b24c-11e9-a2a3-2a2ae2dbcce4"
version = "1.12.0"

[[deps.ChangesOfVariables]]
deps = ["ChainRulesCore", "LinearAlgebra", "Test"]
git-tree-sha1 = "bf98fa45a0a4cee295de98d4c1462be26345b9a1"
uuid = "9e997f8a-9a97-42d5-a9f1-ce6bfc15e2c0"
version = "0.1.2"

[[deps.CodeTracking]]
deps = ["InteractiveUtils", "UUIDs"]
git-tree-sha1 = "9aa8a5ebb6b5bf469a7e0e2b5202cf6f8c291104"
uuid = "da1fd8a2-8d9e-5ec2-8556-3022fb5608a2"
version = "1.0.6"

[[deps.CodecBzip2]]
deps = ["Bzip2_jll", "Libdl", "TranscodingStreams"]
git-tree-sha1 = "2e62a725210ce3c3c2e1a3080190e7ca491f18d7"
uuid = "523fee87-0ab8-5b00-afb7-3ecf72e48cfd"
version = "0.7.2"

[[deps.CodecZlib]]
deps = ["TranscodingStreams", "Zlib_jll"]
git-tree-sha1 = "ded953804d019afa9a3f98981d99b33e3db7b6da"
uuid = "944b1d66-785c-5afd-91f1-9de20f533193"
version = "0.7.0"

[[deps.ColorTypes]]
deps = ["FixedPointNumbers", "Random"]
git-tree-sha1 = "024fe24d83e4a5bf5fc80501a314ce0d1aa35597"
uuid = "3da002f7-5984-5a60-b8a6-cbb66c0b333f"
version = "0.11.0"

[[deps.CommonSubexpressions]]
deps = ["MacroTools", "Test"]
git-tree-sha1 = "7b8a93dba8af7e3b42fecabf646260105ac373f7"
uuid = "bbf7d656-a473-5ed7-a52c-81e309532950"
version = "0.3.0"

[[deps.Compat]]
deps = ["Base64", "Dates", "DelimitedFiles", "Distributed", "InteractiveUtils", "LibGit2", "Libdl", "LinearAlgebra", "Markdown", "Mmap", "Pkg", "Printf", "REPL", "Random", "SHA", "Serialization", "SharedArrays", "Sockets", "SparseArrays", "Statistics", "Test", "UUIDs", "Unicode"]
git-tree-sha1 = "44c37b4636bc54afac5c574d2d02b625349d6582"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "3.41.0"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[deps.DataStructures]]
deps = ["Compat", "InteractiveUtils", "OrderedCollections"]
git-tree-sha1 = "3daef5523dd2e769dad2365274f760ff5f282c7d"
uuid = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
version = "0.18.11"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.DelimitedFiles]]
deps = ["Mmap"]
uuid = "8bb1440f-4735-579b-a4ab-409b98df4dab"

[[deps.DiffResults]]
deps = ["StaticArrays"]
git-tree-sha1 = "c18e98cba888c6c25d1c3b048e4b3380ca956805"
uuid = "163ba53b-c6d8-5494-b064-1a9d43ac40c5"
version = "1.0.3"

[[deps.DiffRules]]
deps = ["IrrationalConstants", "LogExpFunctions", "NaNMath", "Random", "SpecialFunctions"]
git-tree-sha1 = "84083a5136b6abf426174a58325ffd159dd6d94f"
uuid = "b552c78f-8df3-52c6-915a-8e097449b14b"
version = "1.9.1"

[[deps.Distributed]]
deps = ["Random", "Serialization", "Sockets"]
uuid = "8ba89e20-285c-5b6f-9357-94700520ee1b"

[[deps.DocStringExtensions]]
deps = ["LibGit2"]
git-tree-sha1 = "b19534d1895d702889b219c382a6e18010797f0b"
uuid = "ffbed154-4ef7-542d-bbb7-c09d3a79fcae"
version = "0.8.6"

[[deps.Downloads]]
deps = ["ArgTools", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"

[[deps.FileWatching]]
uuid = "7b1f6079-737a-58dc-b8bc-7a2ca5c1b5ee"

[[deps.FixedPointNumbers]]
deps = ["Statistics"]
git-tree-sha1 = "335bfdceacc84c5cdf16aadc768aa5ddfc5383cc"
uuid = "53c48c17-4a7d-5ca2-90c5-79b7896eea93"
version = "0.8.4"

[[deps.ForwardDiff]]
deps = ["CommonSubexpressions", "DiffResults", "DiffRules", "LinearAlgebra", "LogExpFunctions", "NaNMath", "Preferences", "Printf", "Random", "SpecialFunctions", "StaticArrays"]
git-tree-sha1 = "1bd6fc0c344fc0cbee1f42f8d2e7ec8253dda2d2"
uuid = "f6369f11-7733-5829-9624-2563aa707210"
version = "0.10.25"

[[deps.GLPK]]
deps = ["BinaryProvider", "CEnum", "GLPK_jll", "Libdl", "MathOptInterface"]
git-tree-sha1 = "6f4e9754ee93e2b2ff40c0b0a6b4cdffd289190d"
uuid = "60bf3e95-4087-53dc-ae20-288a0d20c6a6"
version = "0.15.3"

[[deps.GLPK_jll]]
deps = ["Artifacts", "GMP_jll", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "fe68622f32828aa92275895fdb324a85894a5b1b"
uuid = "e8aa6df9-e6ca-548a-97ff-1f85fc5b8b98"
version = "5.0.1+0"

[[deps.GMP_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "781609d7-10c4-51f6-84f2-b8444358ff6d"

[[deps.Hyperscript]]
deps = ["Test"]
git-tree-sha1 = "8d511d5b81240fc8e6802386302675bdf47737b9"
uuid = "47d2ed2b-36de-50cf-bf87-49c2cf4b8b91"
version = "0.0.4"

[[deps.HypertextLiteral]]
git-tree-sha1 = "2b078b5a615c6c0396c77810d92ee8c6f470d238"
uuid = "ac1192a8-f4b3-4bfe-ba22-af5b92cd3ab2"
version = "0.9.3"

[[deps.IOCapture]]
deps = ["Logging", "Random"]
git-tree-sha1 = "f7be53659ab06ddc986428d3a9dcc95f6fa6705a"
uuid = "b5f81e59-6552-4d32-b1f0-c071b021bf89"
version = "0.2.2"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.InverseFunctions]]
deps = ["Test"]
git-tree-sha1 = "a7254c0acd8e62f1ac75ad24d5db43f5f19f3c65"
uuid = "3587e190-3f89-42d0-90ee-14403ec27112"
version = "0.1.2"

[[deps.IrrationalConstants]]
git-tree-sha1 = "7fd44fd4ff43fc60815f8e764c0f352b83c49151"
uuid = "92d709cd-6900-40b7-9082-c6be49f344b6"
version = "0.1.1"

[[deps.JLLWrappers]]
deps = ["Preferences"]
git-tree-sha1 = "abc9885a7ca2052a736a600f7fa66209f96506e1"
uuid = "692b3bcd-3c85-4b1f-b108-f13ce0eb3210"
version = "1.4.1"

[[deps.JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "8076680b162ada2a031f707ac7b4953e30667a37"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.2"

[[deps.JuMP]]
deps = ["Calculus", "DataStructures", "ForwardDiff", "JSON", "LinearAlgebra", "MathOptInterface", "MutableArithmetics", "NaNMath", "OrderedCollections", "Printf", "Random", "SparseArrays", "SpecialFunctions", "Statistics"]
git-tree-sha1 = "30bbc998df62c12eee113685c6f4d2ad30a8781c"
uuid = "4076af6c-e467-56ae-b986-b466b2749572"
version = "0.22.2"

[[deps.JuliaInterpreter]]
deps = ["CodeTracking", "InteractiveUtils", "Random", "UUIDs"]
git-tree-sha1 = "b55aae9a2bf436fc797d9c253a900913e0e90178"
uuid = "aa1ae85d-cabe-5617-a682-6adf51b2e16a"
version = "0.9.3"

[[deps.LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"

[[deps.LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"

[[deps.LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[deps.LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.LogExpFunctions]]
deps = ["ChainRulesCore", "ChangesOfVariables", "DocStringExtensions", "InverseFunctions", "IrrationalConstants", "LinearAlgebra"]
git-tree-sha1 = "e5718a00af0ab9756305a0392832c8952c7426c1"
uuid = "2ab3a3ac-af41-5b50-aa03-7779005ae688"
version = "0.3.6"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.LoweredCodeUtils]]
deps = ["JuliaInterpreter"]
git-tree-sha1 = "6b0440822974cab904c8b14d79743565140567f6"
uuid = "6f1432cf-f94c-5a45-995e-cdbf5db27b0b"
version = "2.2.1"

[[deps.MacroTools]]
deps = ["Markdown", "Random"]
git-tree-sha1 = "3d3e902b31198a27340d0bf00d6ac452866021cf"
uuid = "1914dd2f-81c6-5fcd-8719-6d5c9610ff09"
version = "0.5.9"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MathOptInterface]]
deps = ["BenchmarkTools", "CodecBzip2", "CodecZlib", "JSON", "LinearAlgebra", "MutableArithmetics", "OrderedCollections", "Printf", "SparseArrays", "Test", "Unicode"]
git-tree-sha1 = "625f78c57a263e943f525d3860f30e4d200124ab"
uuid = "b8f27783-ece8-5eb3-8dc8-9495eed66fee"
version = "0.10.8"

[[deps.MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"

[[deps.MutableArithmetics]]
deps = ["LinearAlgebra", "SparseArrays", "Test"]
git-tree-sha1 = "842b5ccd156e432f369b204bb704fd4020e383ac"
uuid = "d8a4904e-b15c-11e9-3269-09a3773c0cb0"
version = "0.3.3"

[[deps.NaNMath]]
git-tree-sha1 = "b086b7ea07f8e38cf122f5016af580881ac914fe"
uuid = "77ba4419-2d1f-58cd-9bb1-8ffee604a2e3"
version = "0.3.7"

[[deps.NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[deps.OpenLibm_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "05823500-19ac-5b8b-9628-191a04bc5112"

[[deps.OpenSpecFun_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "13652491f6856acfd2db29360e1bbcd4565d04f1"
uuid = "efe28fd5-8261-553b-a9e1-b2916fc3738e"
version = "0.5.5+0"

[[deps.OrderedCollections]]
git-tree-sha1 = "85f8e6578bf1f9ee0d11e7bb1b1456435479d47c"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.4.1"

[[deps.Parsers]]
deps = ["Dates"]
git-tree-sha1 = "0b5cfbb704034b5b4c1869e36634438a047df065"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "2.2.1"

[[deps.Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"

[[deps.PlutoUI]]
deps = ["AbstractPlutoDingetjes", "Base64", "ColorTypes", "Dates", "Hyperscript", "HypertextLiteral", "IOCapture", "InteractiveUtils", "JSON", "Logging", "Markdown", "Random", "Reexport", "UUIDs"]
git-tree-sha1 = "8979e9802b4ac3d58c503a20f2824ad67f9074dd"
uuid = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
version = "0.7.34"

[[deps.Preferences]]
deps = ["TOML"]
git-tree-sha1 = "2cf929d64681236a2e074ffafb8d568733d2e6af"
uuid = "21216c6a-2e73-6563-6e65-726566657250"
version = "1.2.3"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.Profile]]
deps = ["Printf"]
uuid = "9abbd945-dff8-562f-b5e8-e1ebf5ef1b79"

[[deps.REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.Reexport]]
git-tree-sha1 = "45e428421666073eab6f2da5c9d310d99bb12f9b"
uuid = "189a3867-3050-52da-a836-e630ba90ab69"
version = "1.2.2"

[[deps.Requires]]
deps = ["UUIDs"]
git-tree-sha1 = "838a3a4188e2ded87a4f9f184b4b0d78a1e91cb7"
uuid = "ae029012-a4dd-5104-9daa-d747884805df"
version = "1.3.0"

[[deps.Revise]]
deps = ["CodeTracking", "Distributed", "FileWatching", "JuliaInterpreter", "LibGit2", "LoweredCodeUtils", "OrderedCollections", "Pkg", "REPL", "Requires", "UUIDs", "Unicode"]
git-tree-sha1 = "2f9d4d6679b5f0394c52731db3794166f49d5131"
uuid = "295af30f-e4ad-537b-8983-00126c2a3abe"
version = "3.3.1"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.SharedArrays]]
deps = ["Distributed", "Mmap", "Random", "Serialization"]
uuid = "1a1011a3-84de-559e-8e89-a11a2f7dc383"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.SpecialFunctions]]
deps = ["ChainRulesCore", "IrrationalConstants", "LogExpFunctions", "OpenLibm_jll", "OpenSpecFun_jll"]
git-tree-sha1 = "8d0c8e3d0ff211d9ff4a0c2307d876c99d10bdf1"
uuid = "276daf66-3868-5448-9aa4-cd146d93841b"
version = "2.1.2"

[[deps.StaticArrays]]
deps = ["LinearAlgebra", "Random", "Statistics"]
git-tree-sha1 = "a635a9333989a094bddc9f940c04c549cd66afcf"
uuid = "90137ffa-7385-5640-81b9-e52037218182"
version = "1.3.4"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"

[[deps.Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"

[[deps.Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[deps.TranscodingStreams]]
deps = ["Random", "Test"]
git-tree-sha1 = "216b95ea110b5972db65aa90f88d8d89dcb8851c"
uuid = "3bb67fe8-82b1-5028-8e26-92a6c54297fa"
version = "0.9.6"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"

[[deps.nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"

[[deps.p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
"""

# ╔═╡ Cell order:
# ╠═026974f2-89a4-11ec-1973-f5579e75c550
# ╠═77d6aca4-b37c-45f0-9b0d-79b6ae5720dd
# ╠═88b45368-2f1b-4b60-b918-bb7c6764c3ba
# ╟─e17062a7-fd7a-4cdc-ab4d-646fc0d73f1b
# ╠═f23529fa-5450-4a60-ad3c-c87bfc6937bf
# ╠═528f1c13-8a6d-4a8d-b547-fbf112ba8bc7
# ╠═e7785811-0df2-4231-8c8a-de4799959fab
# ╠═a5a7abbf-930c-4d69-98ba-f154098c7b31
# ╠═12249dc6-3e84-4801-962d-ee5445b6295a
# ╠═de2d3149-37d4-4877-a9ce-1f9c76a75afd
# ╠═0ddbcac0-b50d-4537-a816-2422e69dcc1b
# ╠═9b944cc4-a1c8-4eba-a9b8-d5db5d18b5ab
# ╟─ae8b7b24-a907-4c7d-9964-b0544d1518e4
# ╠═1559f1a5-e2a8-4ddf-b102-acc83c997fb5
# ╠═0738d3ea-9d80-42b6-9ebf-e29ce2eb0b3f
# ╠═25fc85de-0ba6-4f61-8046-032c13c0f20d
# ╠═36b71f92-fbc3-4673-b97a-e4cce174f5d9
# ╟─3417c645-d1e8-4e3a-9a9f-b667a51b4f1e
# ╠═811d3589-0038-4325-8165-147a162a876b
# ╠═12724bdf-a1b7-415b-a51b-09591f38e8e4
# ╠═5de97ae3-539c-4f13-8a1f-da36db0c598b
# ╠═2962d13d-1f0b-489c-a592-c19f9db14342
# ╠═0095f925-c280-4f6b-be22-aabc2a251814
# ╠═afeece53-ad04-42be-b330-e1fba9dbaf13
# ╠═4b6d45fd-7247-4ede-a96f-908a70488282
# ╟─f627ab7f-62e2-4403-929b-58c647d0bc92
# ╠═50c5bc9c-88f9-45b0-bb4c-a4a5156d995e
# ╠═871b936e-1e5c-4e09-bb0e-fe57f75a4a68
# ╟─66dc45aa-9f86-47b1-a581-657c5df275a2
# ╠═1f675f9f-89b5-4e80-ba5d-cc16057a6212
# ╠═a0cb8700-3f6b-41e1-8fdf-524695d7a0cf
# ╟─0550d0c8-0678-43c3-95b1-176c64d0d41c
# ╟─6072a52a-a8ac-4658-8e24-1a6710f81d0f
# ╠═aa497cc9-b097-4789-bddb-4ac39f90f7f7
# ╠═7a8c8c7a-d74b-4aaa-a0c3-31826a4ba386
# ╟─78059c2c-28d8-4a5b-9f56-bbf3e6a987b3
# ╟─cda93622-2a0b-4018-bbc9-272f79637ad5
# ╟─ed86d971-d772-4d56-ad27-781f2423dcef
# ╠═10eec671-6235-45b2-b2cd-2365193a554d
# ╠═703caa00-c67b-4628-b0f4-a684fa2a3a98
# ╟─3f89a4b7-d9d9-439d-847e-a37159ca0de1
# ╠═5e4269bf-eddc-44a5-975b-0f753b8500f8
# ╠═93610e18-df31-4e7b-bf01-e3bf3fea32ca
# ╟─71b0fabc-a10c-4fe9-849c-30fa4a81d1d8
# ╠═a8f9110a-faf8-42d1-8e71-c83666b2d267
# ╠═53ef74a7-2272-439f-b41e-1fdc20de106b
# ╠═00d13868-dbc9-40db-9820-ea9654ac21db
# ╠═6af01c1b-fc1e-45b5-8c9e-0e788e712f3f
# ╟─b34a3226-aa17-4d1e-8b47-58c0e0f41d67
# ╠═f36529b3-8f80-40ee-a5e3-37f3ef07e268
# ╠═93ce5f3e-21e5-4152-8a2f-5f2541616237
# ╠═c842f225-69c0-4d8e-a93a-823826da08b0
# ╟─c98a7b82-43cb-4738-a8af-01743745d0d2
# ╠═fc45566a-cdca-4ab2-96bd-d59d9f95b008
# ╠═5fe8c2ac-8aec-4815-9487-d89edb88d006
# ╠═74e33699-7b97-45c2-84e9-107eae7c8132
# ╟─882abc98-e7ed-465f-8b05-b8ce99ccbfc5
# ╠═d7fb97d6-5337-43ec-aa47-e84dcd0c971e
# ╠═5a634ca6-1280-447d-a8fd-dd800a75b3c2
# ╠═0e75c336-4a51-4b21-a713-0f91e58c6b66
# ╟─a9cd7f50-913f-48cd-a7e2-2009deb08121
# ╠═f9dc9abc-597c-4292-88ac-e6c8db3affd3
# ╠═3d4bf03e-2533-449e-8b58-b70ef5f343cf
# ╠═05e4bd94-fa57-49b3-85f3-dc4980cb66b6
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
