### A Pluto.jl notebook ###
# v0.17.5

using Markdown
using InteractiveUtils

# ╔═╡ f9252aa0-b7f2-11ec-19c8-4f8d84829f6b
using Revise

# ╔═╡ b8907521-c6fe-46f4-adbf-25bf96edfcf5
using PlutoUI

# ╔═╡ e45ad44c-131e-468c-b528-c2d3a7d2ef5a
using DelimitedFiles

# ╔═╡ dd245c7d-ce4f-4551-b13f-dc32c101fb56
using DataFrames

# ╔═╡ f819a45d-913d-481d-9d1f-0654f9d5ce28
TableOfContents(depth=4)

# ╔═╡ 4a99dac5-802e-4bbb-8fbf-3e2a34fd3443
md"""
# 
### Тема: Асоціативні првила

#### Мета: Навчитися використанню асоціативних правил для знаходження закономірностей між зв'язаними подіями

#### Виконав: Юрій Харченко

##### В роботі використано пакети мови Julia - Pluto, https://github.com/JuliaHealth/ARules.jl

"""

# ╔═╡ 66ef0e17-d4c1-45b3-a635-4318c6df8c45
data, header = readdlm("market-utf8.txt", '\t', header = true);

# ╔═╡ eed71130-3662-4816-aa87-1614186fcb46
DataFrame(data, vec(header))

# ╔═╡ 3bc62225-562b-4eaf-9c3d-561826b46b4a
nrows = size(data, 1)

# ╔═╡ dfb98ccd-31cc-4ed6-bd0d-eb5c434432c2
ncols = size(data, 2)

# ╔═╡ a83098bf-82e3-4c5c-ac4f-1ce69e889569
dict = Dict()

# ╔═╡ 97f8437b-7d11-42c7-96a6-8c908e24ddc0
for i = 1:nrows
	k = data[i, 1]
	v = data[i, 2]
	if haskey(dict, k)
		e = dict[k]
		push!(e, v)
	else
		e = [v]
	end
	dict[k] = e
end

# ╔═╡ 53a619da-6807-4922-877e-2b0df495f7d1
transactions = Array{Vector{String}}(collect(values(dict)))

# ╔═╡ c0534272-084c-42df-9477-0792845ac7da
struct Node
    id::Int16
    item_ids::Array{Int16,1}
    transactions::BitArray{1}
    children::Array{Node,1}
    mother::Node
    supp::Int

    function Node(id::Int16, item_ids::Array{Int16,1}, transactions::BitArray{1})
        children = Array{Node,1}(undef, 0)
        nd = new(id, item_ids, transactions, children)
        return nd
    end

    function Node(id::Int16, item_ids::Array{Int16,1}, transactions::BitArray{1}, mother::Node, supp::Int)
        children = Array{Node,1}(undef, 0)
        nd = new(id, item_ids, transactions, children, mother, supp)
        return nd
    end
end

# ╔═╡ 78243333-7485-4c11-a077-ac2a5c0c7e0c
struct Rule
    p::Array{Int16,1}
    q::Int16
    supp::Float64
    conf::Float64
    lift::Float64
end

# ╔═╡ 18a6badf-cadc-4c5e-a08d-9129c0bb2cab
function gen_node_rules(node::Node, supp_dict::Dict{Array{Int16,1}, Int}, k, num_transacts, minconf)
    lhs_keep = trues(k)
    rules = Array{Rule, 1}(undef, 0)
    for i = 1:k
        lhs_keep[i] = false
        if i > 1
            lhs_keep[i-1] = true
        end
        p = node.item_ids[lhs_keep]
        supp = node.supp/num_transacts
        # for debugging
        if supp == 0.0
            warn("zero supp for: ", node.item_ids)
            return rules            # NOTE: returning early if zero support
        else
            conf = supp/((supp_dict[node.item_ids[lhs_keep]])/num_transacts)
            if conf ≥ minconf
                rhs_keep = .!lhs_keep
                q = first(node.item_ids[rhs_keep])
                lift = conf/((supp_dict[node.item_ids[rhs_keep]])/num_transacts)

                rule = Rule(p, q, supp, conf, lift)
                push!(rules, rule)
            end
        end
    end
    rules
end

# ╔═╡ 9006374d-9083-4ff1-901f-5dfa47610db6
function gen_rules!(rules::Array{Rule, 1}, node::Node, supp_dict::Dict{Array{Int16, 1}, Int}, k, num_transacts, minconf)
    m = length(node.children)

    for child in node.children
        rules_tmp = gen_node_rules(child, supp_dict, k, num_transacts, minconf)
        append!(rules, rules_tmp)
        if !isempty(child.children)
            gen_rules!(rules, child, supp_dict, k+1, num_transacts, minconf)
        end
    end
end

# ╔═╡ 993acf7d-1bad-450e-8eb1-f628c1f8b267
function gen_rules(root::Node, supp_dict::Dict{Array{Int16, 1}, Int}, num_transacts, minconf)
    rules = Array{Rule, 1}(undef, 0)
    n_kids = length(root.children)
    if n_kids > 0
        for i = 1:n_kids
            gen_rules!(rules, root.children[i], supp_dict, 2, num_transacts, minconf)
        end
    end
    rules
end

# ╔═╡ 21495cc4-c838-412f-9b37-e8c1c8c90a9a
function rules_to_dataframe(rules::Array{Rule, 1}, item_lkup::Dict{T, S}; join_str = " | ") where {T <: Integer, S}
    n_rules = length(rules)
    dt = DataFrame(lhs = fill("", n_rules),
                   rhs = fill("", n_rules),
                   supp = zeros(n_rules),
                   conf = zeros(n_rules),
                   lift = zeros(n_rules))
    for i = 1:n_rules
        lhs_items = map(x -> string.(item_lkup[x]), rules[i].p)

        lhs_string = "{" * join(lhs_items, join_str) * "}"
        dt[i, :lhs] = lhs_string
        dt[i, :rhs] = string.(item_lkup[rules[i].q])
        dt[i, :supp] = rules[i].supp
        dt[i, :conf] = rules[i].conf
        dt[i, :lift] = rules[i].lift
    end
    dt
end

# ╔═╡ 07851508-d508-42d9-8f82-ac68faee168d
function unique_items(transactions::Array{Array{M, 1}, 1}) where {M}
    dict = Dict{M, Bool}()

    for t in transactions
        for i in t
            dict[i] = true
        end
    end
    uniq_items = collect(keys(dict))
    return sort(uniq_items)
end


# ╔═╡ da01a2d1-5ace-413c-931f-49137b449aa4
function suppdict_to_dataframe(supp_lkup, item_lkup)
    n_sets = length(supp_lkup)
    df = DataFrame(itemset = fill("", n_sets), supp = zeros(Int, n_sets))
    i = 1

    for (k, v) in supp_lkup
        item_names = map(x -> item_lkup[x], k)
        itemset_string = "{" * join(item_names, " | ") * "}"
        df[i, :itemset] = itemset_string
        df[i, :supp] = v
        i += 1
    end
    df
end

# ╔═╡ 20fb5fd9-9ba3-45e3-87c7-d0e7d3870455
function occurrence(transactions::Array{Array{S, 1}, 1}, uniq_items::Array{S, 1}) where S
    n = length(transactions)
    p = length(uniq_items)

    itm_pos = Dict(zip(uniq_items, 1:p))
    res = falses(n, p)
    for i = 1:n
        for itm in transactions[i]
            j = itm_pos[itm]
            res[i, j] = true
        end
    end
    res
end

# ╔═╡ af425d52-edd3-4123-bf07-e0c7ed17b985
function has_children(nd::Node)
    res = length(nd.children) > 0
    res
end

# ╔═╡ be3690b0-e08e-4bcc-835c-79d4f4678c33
function older_siblings(nd::Node)
    n_sibs = length(nd.mother.children)

    sib_indcs = map(x -> x.id > nd.id, nd.mother.children)
    return view(nd.mother.children, sib_indcs)
end

# ╔═╡ f1bdc7be-1fd2-40b1-9d77-5472a53e7cea
function growtree!(nd::Node, minsupp, k, maxdepth)
    sibs = older_siblings(nd)

    for j = 1:length(sibs)
        transacts = nd.transactions .& sibs[j].transactions
        supp = sum(transacts)

        if supp ≥ minsupp
            items = zeros(Int16, k)
            items[1:k-1] = nd.item_ids[1:k-1]
            items[end] = sibs[j].item_ids[end]

            child = Node(Int16(j), items, transacts, nd, supp)
            push!(nd.children, child)
        end
    end
    # Recurse on newly created children
    maxdepth -= 1
    if maxdepth > 1
        for kid in nd.children
            growtree!(kid, minsupp, k+1, maxdepth)
        end
    end
end

# ╔═╡ 12c49ecb-bd61-4590-a513-02842b451b31
function frequent_item_tree(transactions::Array{Array{S, 1}, 1}, uniq_items::Array{S, 1}, minsupp::Int, maxdepth::Int) where S
    occ = occurrence(transactions, uniq_items)

    # Have to initialize `itms` array like this because type inference
    # seems to be broken for this otherwise (using v0.6.0)
    itms = Array{Int16,1}(undef, 1)
    itms[1] = -1
    id = Int16(1)
    transacts = BitArray(undef, 0)
    root = Node(id, itms, transacts)
    n_items = length(uniq_items)

    # This loop creates 1-item nodes (i.e., first children)
    for j = 1:n_items
        supp = sum(occ[:, j])
        if supp ≥ minsupp
            nd = Node(Int16(j), Int16[j], occ[:, j], root, supp)
            push!(root.children, nd)
        end
    end
    n_kids = length(root.children)

    # Grow nodes in breadth-first manner
    for j = 1:n_kids
        growtree!(root.children[j], minsupp, 2, maxdepth)
    end
    root
end

# ╔═╡ 7d9986ac-6719-4924-aee8-04f95068ed79
function update_support_cnt!(supp_dict::Dict, nd::Node)
    supp_dict[nd.item_ids] = nd.supp
end

# ╔═╡ f9619a5f-49da-4545-affb-9e04d6054b12
function grow_support_dict!(supp_cnt::Dict{Array{Int16,1}, Int}, node::Node)
    if has_children(node)
        for nd in node.children
            update_support_cnt!(supp_cnt, nd)
            grow_support_dict!(supp_cnt, nd)
        end
    end
end

# ╔═╡ 92f11e1b-1460-4e7a-80a9-b7d0be4895a5
function gen_support_dict(root::Node, num_transacts)
    supp_cnt = Dict{Array{Int16, 1}, Int}()
    supp_cnt[Int16[]] = num_transacts
    grow_support_dict!(supp_cnt, root)
    return supp_cnt
end


# ╔═╡ e4189fb7-8b95-438f-8614-7fe2a3f736e8
"""
    frequent()
This function just acts as a bit of a convenience function that returns the frequent
item sets and their support count (integer) when given and array of transactions. It
basically just wraps frequent_item_tree() but gives back the plain text of the items,
rather than that Int16 representation.
"""
function frequent(transactions::Array{Array{S, 1}, 1}, minsupp::T, maxdepth) where {T <: Real, S}
    n = length(transactions)
    uniq_items = unique_items(transactions)
    item_lkup = Dict{Int16, S}()
    for (i, itm) in enumerate(uniq_items)
        item_lkup[i] = itm
    end
    if T <: Integer
        supp = minsupp
    elseif T == Float64
        supp = floor(Int, minsupp * n)
        if supp == 0
            warn("Minimum support should not be 0; setting it to 1 now.")
            supp = 1
        end
    end
    freq_tree = frequent_item_tree(transactions, uniq_items, supp, maxdepth)

    supp_lkup = gen_support_dict(freq_tree, n)

    freq = suppdict_to_dataframe(supp_lkup, item_lkup)
    return freq
end

# ╔═╡ a1a6cefa-4728-4950-9264-24cf8730b952
frequent(transactions, 2, 6)

# ╔═╡ 2635237f-2c44-4e84-8daa-e0ce0364e5dc
"""
    apriori(transactions; supp, conf, maxlen)
Given an array of transactions (a vector of string vectors), this function runs the a-priori
algorithm for generating frequent item sets. These frequent items are then used to generate
association rules. The `supp` argument allows us to stipulate the minimum support
required for an itemset to be considered frequent. The `conf` argument allows us to exclude
association rules without at least `conf` level of confidence. The `maxlen` argument stipulates
the maximum length of an association rule (i.e., total items on left- and right-hand sides)
"""
function apriori(transactions::Array{Array{S, 1}, 1}; supp::Float64 = 0.01, conf = 0.8, maxlen::Int = 5) where S
    n = length(transactions)
    uniq_items = unique_items(transactions)
    item_lkup = Dict{Int16, S}()
    for (i, itm) in enumerate(uniq_items)
        item_lkup[i] = itm
    end
    minsupp = floor(Int, supp * n)
    if minsupp == 0
        minsupp += 1
    end
    freq_tree = frequent_item_tree(transactions, uniq_items, minsupp, maxlen)
    supp_lkup = gen_support_dict(freq_tree, n)
    rules = gen_rules(freq_tree, supp_lkup, n, conf)
    rules_dt = rules_to_dataframe(rules, item_lkup)
    return rules_dt
end

# ╔═╡ 2d4d8657-dc51-4712-b64e-5217bc5cc04b
apriori(transactions, supp = 0.01, conf = 0.1, maxlen = 6)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
DataFrames = "a93c6f00-e57d-5684-b7b6-d8193f3e46c0"
DelimitedFiles = "8bb1440f-4735-579b-a4ab-409b98df4dab"
PlutoUI = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
Revise = "295af30f-e4ad-537b-8983-00126c2a3abe"

[compat]
DataFrames = "~1.3.2"
PlutoUI = "~0.7.38"
Revise = "~3.3.3"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.1"
manifest_format = "2.0"

[[deps.AbstractPlutoDingetjes]]
deps = ["Pkg"]
git-tree-sha1 = "8eaf9f1b4921132a4cff3f36a1d9ba923b14a481"
uuid = "6e696c72-6542-2067-7265-42206c756150"
version = "1.1.4"

[[deps.ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.CodeTracking]]
deps = ["InteractiveUtils", "UUIDs"]
git-tree-sha1 = "9fb640864691a0936f94f89150711c36072b0e8f"
uuid = "da1fd8a2-8d9e-5ec2-8556-3022fb5608a2"
version = "1.0.8"

[[deps.ColorTypes]]
deps = ["FixedPointNumbers", "Random"]
git-tree-sha1 = "024fe24d83e4a5bf5fc80501a314ce0d1aa35597"
uuid = "3da002f7-5984-5a60-b8a6-cbb66c0b333f"
version = "0.11.0"

[[deps.Compat]]
deps = ["Base64", "Dates", "DelimitedFiles", "Distributed", "InteractiveUtils", "LibGit2", "Libdl", "LinearAlgebra", "Markdown", "Mmap", "Pkg", "Printf", "REPL", "Random", "SHA", "Serialization", "SharedArrays", "Sockets", "SparseArrays", "Statistics", "Test", "UUIDs", "Unicode"]
git-tree-sha1 = "96b0bc6c52df76506efc8a441c6cf1adcb1babc4"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "3.42.0"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[deps.Crayons]]
git-tree-sha1 = "249fe38abf76d48563e2f4556bebd215aa317e15"
uuid = "a8cc5b0e-0ffa-5ad4-8c14-923d3ee1735f"
version = "4.1.1"

[[deps.DataAPI]]
git-tree-sha1 = "cc70b17275652eb47bc9e5f81635981f13cea5c8"
uuid = "9a962f9c-6df0-11e9-0e5d-c546b8b5ee8a"
version = "1.9.0"

[[deps.DataFrames]]
deps = ["Compat", "DataAPI", "Future", "InvertedIndices", "IteratorInterfaceExtensions", "LinearAlgebra", "Markdown", "Missings", "PooledArrays", "PrettyTables", "Printf", "REPL", "Reexport", "SortingAlgorithms", "Statistics", "TableTraits", "Tables", "Unicode"]
git-tree-sha1 = "ae02104e835f219b8930c7664b8012c93475c340"
uuid = "a93c6f00-e57d-5684-b7b6-d8193f3e46c0"
version = "1.3.2"

[[deps.DataStructures]]
deps = ["Compat", "InteractiveUtils", "OrderedCollections"]
git-tree-sha1 = "3daef5523dd2e769dad2365274f760ff5f282c7d"
uuid = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
version = "0.18.11"

[[deps.DataValueInterfaces]]
git-tree-sha1 = "bfc1187b79289637fa0ef6d4436ebdfe6905cbd6"
uuid = "e2d170a0-9d28-54be-80f0-106bbe20a464"
version = "1.0.0"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.DelimitedFiles]]
deps = ["Mmap"]
uuid = "8bb1440f-4735-579b-a4ab-409b98df4dab"

[[deps.Distributed]]
deps = ["Random", "Serialization", "Sockets"]
uuid = "8ba89e20-285c-5b6f-9357-94700520ee1b"

[[deps.Downloads]]
deps = ["ArgTools", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"

[[deps.FileWatching]]
uuid = "7b1f6079-737a-58dc-b8bc-7a2ca5c1b5ee"

[[deps.FixedPointNumbers]]
deps = ["Statistics"]
git-tree-sha1 = "335bfdceacc84c5cdf16aadc768aa5ddfc5383cc"
uuid = "53c48c17-4a7d-5ca2-90c5-79b7896eea93"
version = "0.8.4"

[[deps.Formatting]]
deps = ["Printf"]
git-tree-sha1 = "8339d61043228fdd3eb658d86c926cb282ae72a8"
uuid = "59287772-0a20-5a39-b81b-1366585eb4c0"
version = "0.4.2"

[[deps.Future]]
deps = ["Random"]
uuid = "9fa8497b-333b-5362-9e8d-4d0656e87820"

[[deps.Hyperscript]]
deps = ["Test"]
git-tree-sha1 = "8d511d5b81240fc8e6802386302675bdf47737b9"
uuid = "47d2ed2b-36de-50cf-bf87-49c2cf4b8b91"
version = "0.0.4"

[[deps.HypertextLiteral]]
git-tree-sha1 = "2b078b5a615c6c0396c77810d92ee8c6f470d238"
uuid = "ac1192a8-f4b3-4bfe-ba22-af5b92cd3ab2"
version = "0.9.3"

[[deps.IOCapture]]
deps = ["Logging", "Random"]
git-tree-sha1 = "f7be53659ab06ddc986428d3a9dcc95f6fa6705a"
uuid = "b5f81e59-6552-4d32-b1f0-c071b021bf89"
version = "0.2.2"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.InvertedIndices]]
git-tree-sha1 = "bee5f1ef5bf65df56bdd2e40447590b272a5471f"
uuid = "41ab1584-1d38-5bbf-9106-f11c6c58b48f"
version = "1.1.0"

[[deps.IteratorInterfaceExtensions]]
git-tree-sha1 = "a3f24677c21f5bbe9d2a714f95dcd58337fb2856"
uuid = "82899510-4779-5014-852e-03e436cf321d"
version = "1.0.0"

[[deps.JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "3c837543ddb02250ef42f4738347454f95079d4e"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.3"

[[deps.JuliaInterpreter]]
deps = ["CodeTracking", "InteractiveUtils", "Random", "UUIDs"]
git-tree-sha1 = "cd6ce9cee498f6b044357cb439f2a3fea3d450df"
uuid = "aa1ae85d-cabe-5617-a682-6adf51b2e16a"
version = "0.9.12"

[[deps.LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"

[[deps.LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"

[[deps.LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[deps.LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.LoweredCodeUtils]]
deps = ["JuliaInterpreter"]
git-tree-sha1 = "6b0440822974cab904c8b14d79743565140567f6"
uuid = "6f1432cf-f94c-5a45-995e-cdbf5db27b0b"
version = "2.2.1"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"

[[deps.Missings]]
deps = ["DataAPI"]
git-tree-sha1 = "bf210ce90b6c9eed32d25dbcae1ebc565df2687f"
uuid = "e1d29d7a-bbdc-5cf2-9ac0-f12de2c33e28"
version = "1.0.2"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"

[[deps.NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[deps.OrderedCollections]]
git-tree-sha1 = "85f8e6578bf1f9ee0d11e7bb1b1456435479d47c"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.4.1"

[[deps.Parsers]]
deps = ["Dates"]
git-tree-sha1 = "621f4f3b4977325b9128d5fae7a8b4829a0c2222"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "2.2.4"

[[deps.Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"

[[deps.PlutoUI]]
deps = ["AbstractPlutoDingetjes", "Base64", "ColorTypes", "Dates", "Hyperscript", "HypertextLiteral", "IOCapture", "InteractiveUtils", "JSON", "Logging", "Markdown", "Random", "Reexport", "UUIDs"]
git-tree-sha1 = "670e559e5c8e191ded66fa9ea89c97f10376bb4c"
uuid = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
version = "0.7.38"

[[deps.PooledArrays]]
deps = ["DataAPI", "Future"]
git-tree-sha1 = "28ef6c7ce353f0b35d0df0d5930e0d072c1f5b9b"
uuid = "2dfb63ee-cc39-5dd5-95bd-886bf059d720"
version = "1.4.1"

[[deps.PrettyTables]]
deps = ["Crayons", "Formatting", "Markdown", "Reexport", "Tables"]
git-tree-sha1 = "dfb54c4e414caa595a1f2ed759b160f5a3ddcba5"
uuid = "08abe8d2-0d0c-5749-adfa-8a2ac140af0d"
version = "1.3.1"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.Reexport]]
git-tree-sha1 = "45e428421666073eab6f2da5c9d310d99bb12f9b"
uuid = "189a3867-3050-52da-a836-e630ba90ab69"
version = "1.2.2"

[[deps.Requires]]
deps = ["UUIDs"]
git-tree-sha1 = "838a3a4188e2ded87a4f9f184b4b0d78a1e91cb7"
uuid = "ae029012-a4dd-5104-9daa-d747884805df"
version = "1.3.0"

[[deps.Revise]]
deps = ["CodeTracking", "Distributed", "FileWatching", "JuliaInterpreter", "LibGit2", "LoweredCodeUtils", "OrderedCollections", "Pkg", "REPL", "Requires", "UUIDs", "Unicode"]
git-tree-sha1 = "4d4239e93531ac3e7ca7e339f15978d0b5149d03"
uuid = "295af30f-e4ad-537b-8983-00126c2a3abe"
version = "3.3.3"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.SharedArrays]]
deps = ["Distributed", "Mmap", "Random", "Serialization"]
uuid = "1a1011a3-84de-559e-8e89-a11a2f7dc383"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SortingAlgorithms]]
deps = ["DataStructures"]
git-tree-sha1 = "b3363d7460f7d098ca0912c69b082f75625d7508"
uuid = "a2af1166-a08f-5f64-846c-94a0d3cef48c"
version = "1.0.1"

[[deps.SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"

[[deps.TableTraits]]
deps = ["IteratorInterfaceExtensions"]
git-tree-sha1 = "c06b2f539df1c6efa794486abfb6ed2022561a39"
uuid = "3783bdb8-4a98-5b6b-af9a-565f29a5fe9c"
version = "1.0.1"

[[deps.Tables]]
deps = ["DataAPI", "DataValueInterfaces", "IteratorInterfaceExtensions", "LinearAlgebra", "OrderedCollections", "TableTraits", "Test"]
git-tree-sha1 = "5ce79ce186cc678bbb5c5681ca3379d1ddae11a1"
uuid = "bd369af6-aec1-5ad0-b16a-f7cc5008161c"
version = "1.7.0"

[[deps.Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"

[[deps.Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"

[[deps.nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"

[[deps.p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
"""

# ╔═╡ Cell order:
# ╠═f9252aa0-b7f2-11ec-19c8-4f8d84829f6b
# ╠═b8907521-c6fe-46f4-adbf-25bf96edfcf5
# ╠═f819a45d-913d-481d-9d1f-0654f9d5ce28
# ╠═4a99dac5-802e-4bbb-8fbf-3e2a34fd3443
# ╠═e45ad44c-131e-468c-b528-c2d3a7d2ef5a
# ╠═dd245c7d-ce4f-4551-b13f-dc32c101fb56
# ╠═66ef0e17-d4c1-45b3-a635-4318c6df8c45
# ╠═eed71130-3662-4816-aa87-1614186fcb46
# ╠═3bc62225-562b-4eaf-9c3d-561826b46b4a
# ╠═dfb98ccd-31cc-4ed6-bd0d-eb5c434432c2
# ╠═a83098bf-82e3-4c5c-ac4f-1ce69e889569
# ╠═97f8437b-7d11-42c7-96a6-8c908e24ddc0
# ╠═53a619da-6807-4922-877e-2b0df495f7d1
# ╠═a1a6cefa-4728-4950-9264-24cf8730b952
# ╠═2d4d8657-dc51-4712-b64e-5217bc5cc04b
# ╠═e4189fb7-8b95-438f-8614-7fe2a3f736e8
# ╠═2635237f-2c44-4e84-8daa-e0ce0364e5dc
# ╠═c0534272-084c-42df-9477-0792845ac7da
# ╠═78243333-7485-4c11-a077-ac2a5c0c7e0c
# ╠═993acf7d-1bad-450e-8eb1-f628c1f8b267
# ╠═9006374d-9083-4ff1-901f-5dfa47610db6
# ╠═18a6badf-cadc-4c5e-a08d-9129c0bb2cab
# ╠═21495cc4-c838-412f-9b37-e8c1c8c90a9a
# ╠═07851508-d508-42d9-8f82-ac68faee168d
# ╠═12c49ecb-bd61-4590-a513-02842b451b31
# ╠═da01a2d1-5ace-413c-931f-49137b449aa4
# ╠═20fb5fd9-9ba3-45e3-87c7-d0e7d3870455
# ╠═f1bdc7be-1fd2-40b1-9d77-5472a53e7cea
# ╠═af425d52-edd3-4123-bf07-e0c7ed17b985
# ╠═be3690b0-e08e-4bcc-835c-79d4f4678c33
# ╠═92f11e1b-1460-4e7a-80a9-b7d0be4895a5
# ╠═f9619a5f-49da-4545-affb-9e04d6054b12
# ╠═7d9986ac-6719-4924-aee8-04f95068ed79
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
