### A Pluto.jl notebook ###
# v0.17.5

using Markdown
using InteractiveUtils

# ╔═╡ 77f21090-a84a-11ec-1dad-71bd919b71d4
using Revise

# ╔═╡ 364a70e4-e08b-473b-842b-d697ecaa13a5
using PlutoUI

# ╔═╡ 666738a2-c7d0-4eab-a516-5b20cf1b9851
TableOfContents(depth=4)

# ╔═╡ 466640e6-e39d-465f-b9b6-466817fa567e
md"""
# 
### Тема: Марковські процеси прийняття рішень

#### Мета: Навчитися застосуванню методів динамічного програмування для вирішення стохастичних задач прийняття рішень

#### Виконав: Юрій Харченко

##### В роботі використано пакети мови Julia - Pluto, JuMP, GLPK
"""

# ╔═╡ 5c27c927-91da-4cf1-9aa8-d2d17913bb57
md"""
# 
#### Приклад Задача садівника (Таха)
"""

# ╔═╡ d1ca82fb-74c1-46ad-9d66-28e4d6610ad8
let

P¹ = [
	0.2 0.5 0.3
	0.0 0.5 0.5
	0.0 0.0 1.0
]

P² = [
	0.3  0.6 0.1
	0.1  0.6 0.3
	0.05 0.4 0.55
]

R¹ = [
	7.0 6.0 3.0
	0.0 5.0 1.0
	0.0 0.0 -1.0
]

R² = [
	6.0 5.0 -1.0
	7.0 4.0 0.0
	6.0 3.0 -2.0
]

ν₁¹ = sum(P¹[1, :].*R¹[1, :])
ν₂¹ = sum(P¹[2, :].*R¹[2, :])
ν₃¹ = sum(P¹[3, :].*R¹[3, :])

ν₁² = sum(P²[1, :].*R²[1, :])
ν₂² = sum(P²[2, :].*R²[2, :])
ν₃² = sum(P²[3, :].*R²[3, :])

maxindex = (x) -> findfirst(==(maximum(x)), x)

f₃ = [
	maximum([ν₁¹, ν₁²]), 
	maximum([ν₂¹, ν₂²]), 
	maximum([ν₃¹, ν₃²])
]

f₂ = [
	maximum([ν₁¹ + sum(P¹[1, :].*f₃), ν₁² + sum(P²[1, :].*f₃)]), 
	maximum([ν₂¹ + sum(P¹[2, :].*f₃), ν₂² + sum(P²[2, :].*f₃)]), 
	maximum([ν₃¹ + sum(P¹[3, :].*f₃), ν₃² + sum(P²[3, :].*f₃)])
]

f₁ = [
	maximum([ν₁¹ + sum(P¹[1, :].*f₂), ν₁² + sum(P²[1, :].*f₂)]), 
	maximum([ν₂¹ + sum(P¹[2, :].*f₂), ν₂² + sum(P²[2, :].*f₂)]), 
	maximum([ν₃¹ + sum(P¹[3, :].*f₂), ν₃² + sum(P²[3, :].*f₂)])
]

md"""
Етап 3

  1 : $(round(ν₁¹, digits=2)), $(round(ν₁², digits=2)), $(round(f₃[1], digits=2)), $(maxindex([ν₁¹, ν₁²]))

  2 : $(round(ν₂¹, digits=2)), $(round(ν₂², digits=2)), $(round(f₃[2], digits=2)), $(maxindex([ν₂¹, ν₂²])) 

  3 : $(round(ν₃¹, digits=2)), $(round(ν₃², digits=2)), $(round(f₃[3], digits=2)), $(maxindex([ν₃¹, ν₃²]))

Етап 2

  1 : $(round(ν₁¹ + sum(P¹[1, :].*f₃), digits=2)), $(round(ν₁² + sum(P²[1, :].*f₃), digits=2)), $(round(f₂[1], digits=2)), $(maxindex([ν₁¹ + sum(P¹[1, :].*f₃), ν₁² + sum(P²[1, :].*f₃)]))

  2 : $(round(ν₂¹ + sum(P¹[2, :].*f₃), digits=2)), $(round(ν₂² + sum(P²[2, :].*f₃), digits=2)), $(round(f₂[2], digits=2)), $(maxindex([ν₂¹ + sum(P¹[2, :].*f₃), ν₂² + sum(P²[2, :].*f₃)])) 

  3 : $(round(ν₃¹ + sum(P¹[3, :].*f₃), digits=2)), $(round(ν₃² + sum(P²[3, :].*f₃), digits=2)), $(round(f₂[3], digits=2)), $(maxindex([ν₃¹ + sum(P¹[3, :].*f₃), ν₃² + sum(P²[3, :].*f₃)]))

Етап 1

  1 : $(round(ν₁¹ + sum(P¹[1, :].*f₂), digits=2)), $(round(ν₁² + sum(P²[1, :].*f₂), digits=2)), $(round(f₁[1], digits=2)), $(maxindex([ν₁¹ + sum(P¹[1, :].*f₂), ν₁² + sum(P²[1, :].*f₂)]))

  2 : $(round(ν₂¹ + sum(P¹[2, :].*f₂), digits=2)), $(round(ν₂² + sum(P²[2, :].*f₂), digits=2)), $(round(f₁[2], digits=2)), $(maxindex([ν₂¹ + sum(P¹[2, :].*f₂), ν₂² + sum(P²[2, :].*f₂)])) 

  3 : $(round(ν₃¹ + sum(P¹[3, :].*f₂), digits=2)), $(round(ν₃² + sum(P²[3, :].*f₂), digits=2)), $(round(f₁[3], digits=2)), $(maxindex([ν₃¹ + sum(P¹[3, :].*f₂), ν₃² + sum(P²[3, :].*f₂)]))


"""
	
end

# ╔═╡ ba60bffb-bc86-404a-8dd0-0020046e06a3
md"""
# 
#### Задача 1

Компанія може провести рекламну акцію за допомогою одного з трьох засобів масової інформації: радіо, телебачення або газети. Тижневі витрати на рекламу за допомогою цих засобів оцінюються в 200, 900 і 300 дол. відповідно.
Компанія оцінює тижневий обсяг збуту своєї продукції за трибальною шкалою
як задовільний (1), хороший (2) і відмінний (3).
"""

# ╔═╡ c8b07ce3-526d-4ede-b11b-b80f3ea23e5b
let

D = [200, 900, 300]
	
P¹ = [
	0.4 0.5 0.1
	0.1 0.7 0.2
	0.1 0.2 0.7
]

P² = [
	0.7 0.2 0.1
	0.3 0.6 0.1
	0.1 0.7 0.2
]

P³ = [
	0.2 0.5 0.3
	0.0 0.7 0.3
	0.0 0.2 0.8
]

R¹ = [
	400.0 520.0 600.0
	300.0 400.0 700.0
	200.0 250.0 500.0
]

R² = [
	1000.0 1300.0 1600.0
	800.0  1000.0 1700.0
	600.0  700.0  1100.0
]

R³ = [
	400.0 530.0 710.0
	350.0 450.0 800.0
	250.0 400.0 650.0
]

ν₁¹ = sum(P¹[1, :].*R¹[1, :]) - D[1]
ν₂¹ = sum(P¹[2, :].*R¹[2, :]) - D[1]
ν₃¹ = sum(P¹[3, :].*R¹[3, :]) - D[1]

ν₁² = sum(P²[1, :].*R²[1, :]) - D[2]
ν₂² = sum(P²[2, :].*R²[2, :]) - D[2]
ν₃² = sum(P²[3, :].*R²[3, :]) - D[2]

ν₁³ = sum(P³[1, :].*R³[1, :]) - D[3]
ν₂³ = sum(P³[2, :].*R³[2, :]) - D[3]
ν₃³ = sum(P³[3, :].*R³[3, :]) - D[3]

maxindex = (x) -> findfirst(==(maximum(x)), x)

f₃ = [
	maximum([ν₁¹, ν₁², ν₁³]), 
	maximum([ν₂¹, ν₂², ν₂³]), 
	maximum([ν₃¹, ν₃², ν₃³])
]

f₂ = [
	maximum(
		[
			ν₁¹ + sum(P¹[1, :].*f₃), 
			ν₁² + sum(P²[1, :].*f₃),
			ν₁³ + sum(P³[1, :].*f₃)
		]), 
	maximum(
		[
			ν₂¹ + sum(P¹[2, :].*f₃), 
			ν₂² + sum(P²[2, :].*f₃),
			ν₂³ + sum(P³[2, :].*f₃)
		]), 
	maximum(
		[
			ν₃¹ + sum(P¹[3, :].*f₃), 
			ν₃² + sum(P²[3, :].*f₃),
			ν₃³ + sum(P³[3, :].*f₃)
		])
]

f₁ = [
	maximum(
		[
			ν₁¹ + sum(P¹[1, :].*f₂), 
			ν₁² + sum(P²[1, :].*f₂),
			ν₁³ + sum(P³[1, :].*f₂)
		]), 
	maximum(
		[
			ν₂¹ + sum(P¹[2, :].*f₂), 
			ν₂² + sum(P²[2, :].*f₂),
			ν₂³ + sum(P³[2, :].*f₂)
		]), 
	maximum(
		[
			ν₃¹ + sum(P¹[3, :].*f₂), 
			ν₃² + sum(P²[3, :].*f₂),
			ν₃³ + sum(P³[3, :].*f₂)
		])
]

md"""
Етап 3

  1 : $(round(ν₁¹, digits=2)), $(round(ν₁², digits=2)), $(round(ν₁³, digits=2)), $(round(f₃[1], digits=2)), $(maxindex([ν₁¹, ν₁², ν₁³]))

  2 : $(round(ν₂¹, digits=2)), $(round(ν₂², digits=2)), $(round(ν₂³, digits=2)), $(round(f₃[2], digits=2)), $(maxindex([ν₂¹, ν₂², ν₂³])) 

  3 : $(round(ν₃¹, digits=2)), $(round(ν₃², digits=2)), $(round(ν₃³, digits=2)), $(round(f₃[3], digits=2)), $(maxindex([ν₃¹, ν₃², ν₃³]))

Етап 2

  1 : $(round(ν₁¹ + sum(P¹[1, :].*f₃), digits=2)), $(round(ν₁² + sum(P²[1, :].*f₃), digits=2)), $(round(ν₁³ + sum(P³[1, :].*f₃), digits=2)), $(round(f₂[1], digits=2)), $(maxindex([ν₁¹ + sum(P¹[1, :].*f₃), ν₁² + sum(P²[1, :].*f₃), ν₁³ + sum(P³[1, :].*f₃)]))

  2 : $(round(ν₂¹ + sum(P¹[2, :].*f₃), digits=2)), $(round(ν₂² + sum(P²[2, :].*f₃), digits=2)), $(round(ν₂³ + sum(P³[2, :].*f₃), digits=2)), $(round(f₂[2], digits=2)), $(maxindex([ν₂¹ + sum(P¹[2, :].*f₃), ν₂² + sum(P²[2, :].*f₃), ν₂³ + sum(P³[2, :].*f₃)])) 

  3 : $(round(ν₃¹ + sum(P¹[3, :].*f₃), digits=2)), $(round(ν₃² + sum(P²[3, :].*f₃), digits=2)), $(round(ν₃³ + sum(P³[3, :].*f₃), digits=2)), $(round(f₂[3], digits=2)), $(maxindex([ν₃¹ + sum(P¹[3, :].*f₃), ν₃² + sum(P²[3, :].*f₃), ν₃³ + sum(P³[3, :].*f₃)]))

Етап 1

  1 : $(round(ν₁¹ + sum(P¹[1, :].*f₂), digits=2)), $(round(ν₁² + sum(P²[1, :].*f₂), digits=2)), $(round(ν₁³ + sum(P³[1, :].*f₂), digits=2)), $(round(f₁[1], digits=2)), $(maxindex([ν₁¹ + sum(P¹[1, :].*f₂), ν₁² + sum(P²[1, :].*f₂), ν₁³ + sum(P³[1, :].*f₂)]))

  2 : $(round(ν₂¹ + sum(P¹[2, :].*f₂), digits=2)), $(round(ν₂² + sum(P²[2, :].*f₂), digits=2)), $(round(ν₂³ + sum(P³[2, :].*f₂), digits=2)), $(round(f₁[2], digits=2)), $(maxindex([ν₂¹ + sum(P¹[2, :].*f₂), ν₂² + sum(P²[2, :].*f₂), ν₂³ + sum(P³[2, :].*f₂)])) 

  3 : $(round(ν₃¹ + sum(P¹[3, :].*f₂), digits=2)), $(round(ν₃² + sum(P²[3, :].*f₂), digits=2)), $(round(ν₃³ + sum(P³[3, :].*f₂), digits=2)), $(round(f₁[3], digits=2)), $(maxindex([ν₃¹ + sum(P¹[3, :].*f₂), ν₃² + sum(P²[3, :].*f₂), ν₃³ + sum(P³[3, :].*f₂)]))

##### Висновок

Оптимальна стратегія: 
  - якщо стан задовільний (1), то використовувати радіо; 
  - якщо стан хороший (2) або відмінний (3), то газети.

"""
	
end

# ╔═╡ e0593b3d-b452-4811-a6b3-20931f834a45
md"""
# 
#### Задача 2

Стан ділянки землі по ряду показників може визначатися рівнями 1, 2
або 3. При посіві соняшнику очікувані доходи дорівнюють відповідно 1000, 400
і 50 грошових одиниць. При посіві цукрових буряків – 300, 200 і 100.

Вирішіть задачу при нескінченному горизонті планування методами
повного перебору, ітерацій по стратегіях і у вигляді задачі лінійного
програмування

"""

# ╔═╡ 16018042-ebc8-4277-8d55-ff1ead77d43f
let

P¹ = [
	0.1 0.3 0.6
	0.0 0.1 0.9
	0.0 0.0 1.0
]

P² = [
	0.5  0.4 0.1
	0.6  0.3 0.1
	0.8  0.2 0.0
]

end

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
PlutoUI = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
Revise = "295af30f-e4ad-537b-8983-00126c2a3abe"

[compat]
PlutoUI = "~0.7.37"
Revise = "~3.3.3"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.1"
manifest_format = "2.0"

[[deps.AbstractPlutoDingetjes]]
deps = ["Pkg"]
git-tree-sha1 = "8eaf9f1b4921132a4cff3f36a1d9ba923b14a481"
uuid = "6e696c72-6542-2067-7265-42206c756150"
version = "1.1.4"

[[deps.ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.CodeTracking]]
deps = ["InteractiveUtils", "UUIDs"]
git-tree-sha1 = "9fb640864691a0936f94f89150711c36072b0e8f"
uuid = "da1fd8a2-8d9e-5ec2-8556-3022fb5608a2"
version = "1.0.8"

[[deps.ColorTypes]]
deps = ["FixedPointNumbers", "Random"]
git-tree-sha1 = "024fe24d83e4a5bf5fc80501a314ce0d1aa35597"
uuid = "3da002f7-5984-5a60-b8a6-cbb66c0b333f"
version = "0.11.0"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.Distributed]]
deps = ["Random", "Serialization", "Sockets"]
uuid = "8ba89e20-285c-5b6f-9357-94700520ee1b"

[[deps.Downloads]]
deps = ["ArgTools", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"

[[deps.FileWatching]]
uuid = "7b1f6079-737a-58dc-b8bc-7a2ca5c1b5ee"

[[deps.FixedPointNumbers]]
deps = ["Statistics"]
git-tree-sha1 = "335bfdceacc84c5cdf16aadc768aa5ddfc5383cc"
uuid = "53c48c17-4a7d-5ca2-90c5-79b7896eea93"
version = "0.8.4"

[[deps.Hyperscript]]
deps = ["Test"]
git-tree-sha1 = "8d511d5b81240fc8e6802386302675bdf47737b9"
uuid = "47d2ed2b-36de-50cf-bf87-49c2cf4b8b91"
version = "0.0.4"

[[deps.HypertextLiteral]]
git-tree-sha1 = "2b078b5a615c6c0396c77810d92ee8c6f470d238"
uuid = "ac1192a8-f4b3-4bfe-ba22-af5b92cd3ab2"
version = "0.9.3"

[[deps.IOCapture]]
deps = ["Logging", "Random"]
git-tree-sha1 = "f7be53659ab06ddc986428d3a9dcc95f6fa6705a"
uuid = "b5f81e59-6552-4d32-b1f0-c071b021bf89"
version = "0.2.2"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "3c837543ddb02250ef42f4738347454f95079d4e"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.3"

[[deps.JuliaInterpreter]]
deps = ["CodeTracking", "InteractiveUtils", "Random", "UUIDs"]
git-tree-sha1 = "9c43a2eb47147a8776ca2ba489f15a9f6f2906f8"
uuid = "aa1ae85d-cabe-5617-a682-6adf51b2e16a"
version = "0.9.11"

[[deps.LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"

[[deps.LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"

[[deps.LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[deps.LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.LoweredCodeUtils]]
deps = ["JuliaInterpreter"]
git-tree-sha1 = "6b0440822974cab904c8b14d79743565140567f6"
uuid = "6f1432cf-f94c-5a45-995e-cdbf5db27b0b"
version = "2.2.1"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"

[[deps.NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[deps.OrderedCollections]]
git-tree-sha1 = "85f8e6578bf1f9ee0d11e7bb1b1456435479d47c"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.4.1"

[[deps.Parsers]]
deps = ["Dates"]
git-tree-sha1 = "85b5da0fa43588c75bb1ff986493443f821c70b7"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "2.2.3"

[[deps.Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"

[[deps.PlutoUI]]
deps = ["AbstractPlutoDingetjes", "Base64", "ColorTypes", "Dates", "Hyperscript", "HypertextLiteral", "IOCapture", "InteractiveUtils", "JSON", "Logging", "Markdown", "Random", "Reexport", "UUIDs"]
git-tree-sha1 = "bf0a1121af131d9974241ba53f601211e9303a9e"
uuid = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
version = "0.7.37"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.Reexport]]
git-tree-sha1 = "45e428421666073eab6f2da5c9d310d99bb12f9b"
uuid = "189a3867-3050-52da-a836-e630ba90ab69"
version = "1.2.2"

[[deps.Requires]]
deps = ["UUIDs"]
git-tree-sha1 = "838a3a4188e2ded87a4f9f184b4b0d78a1e91cb7"
uuid = "ae029012-a4dd-5104-9daa-d747884805df"
version = "1.3.0"

[[deps.Revise]]
deps = ["CodeTracking", "Distributed", "FileWatching", "JuliaInterpreter", "LibGit2", "LoweredCodeUtils", "OrderedCollections", "Pkg", "REPL", "Requires", "UUIDs", "Unicode"]
git-tree-sha1 = "4d4239e93531ac3e7ca7e339f15978d0b5149d03"
uuid = "295af30f-e4ad-537b-8983-00126c2a3abe"
version = "3.3.3"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"

[[deps.Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"

[[deps.Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"

[[deps.nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"

[[deps.p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
"""

# ╔═╡ Cell order:
# ╠═77f21090-a84a-11ec-1dad-71bd919b71d4
# ╠═364a70e4-e08b-473b-842b-d697ecaa13a5
# ╠═666738a2-c7d0-4eab-a516-5b20cf1b9851
# ╠═466640e6-e39d-465f-b9b6-466817fa567e
# ╟─5c27c927-91da-4cf1-9aa8-d2d17913bb57
# ╠═d1ca82fb-74c1-46ad-9d66-28e4d6610ad8
# ╟─ba60bffb-bc86-404a-8dd0-0020046e06a3
# ╠═c8b07ce3-526d-4ede-b11b-b80f3ea23e5b
# ╟─e0593b3d-b452-4811-a6b3-20931f834a45
# ╠═16018042-ebc8-4277-8d55-ff1ead77d43f
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
